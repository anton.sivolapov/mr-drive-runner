﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using UnityEngine.Purchasing;

public class GameShop : MonoBehaviour {

    public Button m_CloseButton;
    public Button m_RestorePurchasesButton;
    public Text m_CoinsText;
    public Text m_PotionsText;
    public GameObject m_StarterPack;
    private int m_StarterPackIndex;
    public GameObject m_DoubleCoins;
    private int m_DoubleCoinsIndex;
    public bool m_Opened;
    public GameObject m_Note;
    public HorizontalScrollSnap m_Scroll;
    public StoreItem[] m_Items;
    private List<ShopElement> m_ListOfPreviews = new List<ShopElement>();

    [HideInInspector]
    public Purchaser m_Purchaser;
    // Use this for initialization
    void Start () {
        m_CloseButton.onClick.AddListener(ClosePopup);
#if UNITY_IOS
        m_RestorePurchasesButton.onClick.AddListener(RestorePurchase);
        m_Note.SetActive(false);
#elif UNITY_EDITOR
        m_RestorePurchasesButton.onClick.AddListener(RestorePurchase);
#else
        m_RestorePurchasesButton.gameObject.SetActive(false);
#endif
        m_Purchaser = FindObjectOfType<Purchaser>();
        // InitPreviews();
        RefreshUI();
    }

    public void InitPreviews(){
        m_ListOfPreviews.Clear();
        GameObject[] tmp = new GameObject[0];
        m_Scroll.RemoveAllChildren(out tmp);
        
        foreach( GameObject g in tmp ){
            Destroy(g);
        }

        int index = 0;
        foreach( StoreItem v in m_Items ){
            if( v.m_ProductId == "starter_pack" && !GlobalGameManager.instance.m_StarterPackAvailable ){
            }else if( v.m_ProductId == "double_coins" && GlobalGameManager.instance.m_DoubleCoinsPurchased  ){
            }else{
                GameObject previewEl = Instantiate( v.m_PreviewPrefab, transform );
                m_Scroll.AddChild(previewEl);

                ShopElement previewComponent = previewEl.GetComponentInChildren<ShopElement>();
                previewComponent.m_ItemIndex = index;
                m_ListOfPreviews.Add(previewComponent);
                index++;
            }
        }
        // RefreshUI();

        m_Scroll.GoToScreen(0);
    }

    void OnEnable(){
        RefreshUI();

        GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("GameShopScreen");
    }
    
    void BuyProduct (string productId) {
        Debug.Log("Buy " + productId);
        m_Purchaser.BuyProductID(productId);
    }

    public void OpenPopup(){
        m_Opened = true;
        gameObject.SetActive(true);
    }

    public void ClosePopup(){
        m_Opened = false;
        gameObject.SetActive(false);
    }

    public void RefreshUI(){
        // if( !GlobalGameManager.instance.m_StarterPackAvailable ){
        //     m_Scroll.RemoveChild(m_StarterPackIndex, out m_StarterPack);
        //     // m_StarterPack.SetActive(false);
        // }
        // if( GlobalGameManager.instance.m_DoubleCoinsPurchased ){
        //     m_Scroll.RemoveChild(m_DoubleCoinsIndex, out m_DoubleCoins);
        //     // m_DoubleCoins.SetActive(false);
        // }
        InitPreviews();
        if( m_CoinsText != null )
            m_CoinsText.text = string.Format("{0:n0}", GlobalGameManager.instance.m_Coins);
        if( m_PotionsText != null )
            m_PotionsText.text = string.Format("{0:n0}", GlobalGameManager.instance.m_Potions);

        if( gameObject != null )
            gameObject.SendMessageUpwards("RefreshAllUI");
    }

    void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)) {
            ClosePopup();
        }
    }

    public void SelectionChaged(){
        foreach( ShopElement el in m_ListOfPreviews ){
            if( el.m_ItemIndex != m_Scroll.CurrentPage )
                el.MakeInactive();
        }
        if( m_ListOfPreviews.Count > m_Scroll.CurrentPage )
            m_ListOfPreviews[m_Scroll.CurrentPage].MakeActive();
    }
    public void RestorePurchase(){
        // GlobalGameManager.instance.OnProductPurchased("starter_pack", GlobalGameManager.instance.m_GameData.m_GameGoods[0]);

        FindObjectOfType<Purchaser>().RestorePurchase();
    }
}
