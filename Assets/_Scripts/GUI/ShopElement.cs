﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Purchasing;
using UnityEngine.UI.Extensions;

public class ShopElement : MonoBehaviour {
    public StoreItem m_StoreItem;
    public Text m_PriceText;
    public Text m_CoinsText;
    public Text m_PotionsText;
    public Text m_Description;
    public int m_ItemIndex;
    public GameObject m_ActiveState;
    public HorizontalScrollSnap m_Scroll;
    public Button m_BuyButton;

    void Start()
    {
        // m_Scroll = GetComponentInParent<HorizontalScrollSnap>();
        m_BuyButton.onClick.AddListener(BuyCurrentProduct);

        Product product = Purchaser.m_StoreController.products.WithID(m_StoreItem.m_ProductId);

        if( product != null ){
            m_PriceText.text = product.metadata.localizedPriceString;
        }else{
            m_PriceText.text = string.Format("${0:n0}", m_StoreItem.m_Price.ToString());
        }
        if( m_CoinsText != null ){
            m_CoinsText.text = string.Format("{0:n0}", m_StoreItem.m_CoinsNum);
        }
        if( m_PotionsText != null ){
            m_PotionsText.text = "x" + m_StoreItem.m_PotionsNum.ToString();
        }
        // if( m_StoreItem.m_ProductId == "coins_1" ){
        //     Destroy(gameObject);
        // }
        // m_Scroll.UpdateVisible();
    }

    public void BuyCurrentProduct()
    {
        gameObject.SendMessageUpwards("BuyProduct", m_StoreItem.m_ProductId);
    }

    public void MakeInactive(){
        m_ActiveState.SetActive(false);
    }

    public void MakeActive(){
        if( !m_ActiveState.activeSelf ){
            m_ActiveState.SetActive(true);
            m_ActiveState.transform.localScale = Vector3.zero;
            iTween.ScaleTo(m_ActiveState, iTween.Hash("scale", Vector3.one, "easeType", "easeInOutQuad", "time", 0.1f ));
        }
    }
}
