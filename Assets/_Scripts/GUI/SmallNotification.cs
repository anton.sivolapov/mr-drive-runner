﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmallNotification : MonoBehaviour {
    public Text m_MainText;

    // Use this for initialization
    public void ShowNotification ( string text, int delay ) {
        m_MainText.text = text;
        gameObject.SetActive(true);
        StartCoroutine(CloseAfterDelay(delay));
    }

    public void CloseNotification () {
        gameObject.SetActive(false);
    }

    IEnumerator CloseAfterDelay(int time)
    {
        yield return new WaitForSeconds(time);
 
        // Code to execute after the delay
        CloseNotification();
    }
}
