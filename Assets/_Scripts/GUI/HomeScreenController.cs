﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#if !UNITY_STANDALONE
using UnityEngine.Advertisements;
using AppAdvisory.VSRATE;
#endif

using UnityEngine.Analytics;

// using GoogleMobileAds.Api;

public class HomeScreenController : MonoBehaviour {
    public GameObject m_HomePage;
    public GameShop m_CoinsShop;
    public GameObject m_VehicleSelect;
    public GameObject m_SettingsScreen;
    public Button m_SoundToggle;
    public MessagePopup m_MessagePopup;
    public Image m_CurrentPreview;

    public Button m_PlayButton;
    public Button m_MultiplayerGameButton;
    public Button m_TutorialButton;
    public Button m_FacebookButton;
    public Button m_ShopButton;
    public Button m_CharSelectButton;
    // public Button m_OfferwallButton;
    public Button m_RankingButton;
    public Button m_AchievementsButton;
    public Button m_CreditsButton;
    public Button m_RemoveAdsButton;
    public CharSelectController m_CharSelectController;

    private GameObject m_CurrentView;
    // private InterstitialAd interstitial;


    // Use this for initialization
    void Start () {
        GetComponent<AudioSource>().volume = 0.2f;
        //set initial view
        m_CurrentView = m_HomePage;

        m_PlayButton.onClick.AddListener(GoToGame);
        m_MultiplayerGameButton.onClick.AddListener(GoToMultiplayerGame);
        m_TutorialButton.onClick.AddListener(GoToTutorial);
        m_FacebookButton.onClick.AddListener(GoToFacebook);
        m_ShopButton.onClick.AddListener(GoToCoinsShop);
        m_CharSelectButton.onClick.AddListener(GoToCharSelect);
#if !UNITY_STANDALONE
        // m_OfferwallButton.onClick.AddListener(ShowRewardedAd);
#endif

        m_RankingButton.onClick.AddListener(GoToRanking);
        // m_AchievementsButton.onClick.AddListener(GoToAchievements);
        m_CreditsButton.onClick.AddListener(ShowCredits);

        m_SoundToggle.GetComponent<ToggleSwitch>().m_IsOn = GlobalGameManager.instance.m_SoundEnabled;
        m_SoundToggle.onClick.AddListener(ToggleSound);
        
        if( Time.realtimeSinceStartup > 3 * 60 && Random.Range( 1, 4 ) > 1 ){
            RequestInterstitial();
        }
        
        GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("HomeScreen");

        GameData gameData = GlobalGameManager.instance.m_GameData;
        m_CurrentPreview.sprite = gameData.m_Vehicles[GlobalGameManager.instance.m_SelectedVehicleIndex].m_PreviewSprite;


#if !UNITY_IOS && !UNITY_EDITOR
        RemoveRemoveAdsButton();
#else
        m_RemoveAdsButton.onClick.AddListener(RemoveAds);
        if( GlobalGameManager.instance.m_AdsRemoverPurchased ){
            RemoveRemoveAdsButton();
        }
#endif
    }

    void RemoveAds(){
        Purchaser m_Purchaser = FindObjectOfType<Purchaser>();
        m_Purchaser.BuyProductID("remove_ads");
    }

    public void RemoveRemoveAdsButton(){
        m_RemoveAdsButton.gameObject.SetActive(false);
    }

    private void ToggleSound(){
        GlobalGameManager.instance.m_SoundEnabled = !GlobalGameManager.instance.m_SoundEnabled;
        m_SoundToggle.GetComponent<ToggleSwitch>().m_IsOn = GlobalGameManager.instance.m_SoundEnabled;
    }

    private void RequestInterstitial()
    {
        // Debug.Log("RequestInterstitial");
        // string adUnitId = "ca-app-pub-5316161378481999/8565945463";

        // Initialize an InterstitialAd.
        // interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        // AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        // interstitial.LoadAd(request);
        // interstitial.OnAdLoaded += HandleOnAdLoaded;
    }   

    // void HandleOnAdLoaded( object sender, System.EventArgs args )
    // {
    //     print("OnAdLoaded event received.");
    //     interstitial.Show();
    // }

    // IEnumerator ShowOrHideAdButton()
    // {
    //     while ( true ){
    //         if (Advertisement.IsReady("rewardedVideo"))
    //         {
    //             Debug.Log("Ads is ready, show button");
    //             m_OfferwallButton.gameObject.SetActive(true);
    //         }else{
    //             Debug.Log("Ads is NOT ready, hide button");
    //             m_OfferwallButton.gameObject.SetActive(false);
    //         }

    //         yield return new WaitForSeconds(1f);
    //     }
    // }

#if !UNITY_STANDALONE
    void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Debug.Log("Ads is ready");
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }else{
            Debug.Log("Ads is NOT ready");
            GlobalGameManager.instance.Notify(LocalizationText.GetText("ads_not_ready", 500));
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                GlobalGameManager.instance.m_Coins += 500;
                GlobalGameManager.instance.Notify(LocalizationText.GetText("addcoins_lbl", 500));

                Analytics.CustomEvent("rewardedVideoWatched", new Dictionary<string, object>
                                      {
                                          { "coins", 500 }
                                      });
                
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                GlobalGameManager.instance.Notify(LocalizationText.GetText("ad_skiped_lbl"));
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                GlobalGameManager.instance.Notify(LocalizationText.GetText("noads_lbl"));
                break;
        }
    }
#endif

    public void GoBackHome () {
        GameData gameData = GlobalGameManager.instance.m_GameData;
        m_CurrentPreview.sprite = gameData.m_Vehicles[GlobalGameManager.instance.m_SelectedVehicleIndex].m_PreviewSprite;
        GoToView(m_HomePage);
        GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("HomeScreen");
    }

    void GoToRanking () {
        // GlobalGameManager.instance.m_AdManager.ShowInterstitialAd();
        GlobalGameManager.instance.ShowLeaderboard();
    }

    void GoToSettings () {
        GoToView(m_SettingsScreen);
    }

    void GoToAchievements () {
        GlobalGameManager.instance.ShowAchievements();
    }

    void GoToGame () {
        // SceneManager.LoadScene ("Game");
        if( GlobalGameManager.instance.m_NumberOfGameSessions < 1 ){
            LoadingScreenManager.LoadSceneWithLoadingInfo(3);
        }else{
            LoadingScreenManager.LoadSceneWithLoadingInfo(1);
        }
    }

    void ShowCredits () {
        GoToSettings();
        GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("CreditsScreen");
    }

    void AskForRating () {
        RateUsManager.ShowRateUsWindows();
    }

    public void GoToTutorial () {
        m_MessagePopup.m_Ok.AddListener (ProceedToTutorial);
        m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedtutorial_lbl"), LocalizationText.GetText("tutorialdesc_lbl"));
    }

    public void GoToFacebook () {
        Debug.Log("go to facebook");
        Application.OpenURL("https://www.facebook.com/mrdriverunner/");
    }

    void ProceedToTutorial(){
        m_MessagePopup.m_Ok.RemoveListener (ProceedToTutorial);
        m_MessagePopup.ClosePopup();
        LoadingScreenManager.LoadScene(3);
    }
    
    //multiplayer
    void GoToMultiplayerGame () {
        m_MessagePopup.m_Ok.AddListener (ProceedToMultiplayer);
        m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedmultiplayer_lbl"), LocalizationText.GetText("multiplayerdesc_lbl"));
    }

    void ProceedToMultiplayer(){
        // SceneManager.LoadScene ("TwoPlayersGame");
        m_MessagePopup.m_Ok.RemoveListener (ProceedToMultiplayer);
        GlobalGameManager.instance.m_IsMultiplayerGame = true;
        m_MessagePopup.ClosePopup();
        LoadingScreenManager.LoadScene(1);
    }

    void GoToCoinsShop () {
        // GlobalGameManager.instance.m_AdManager.ShowRewardBasedAd( ()=> GlobalGameManager.instance.Notify("You are rewarded!") );
        m_CoinsShop.OpenPopup();
    }

    void GoToCharSelect () {
        GoToView(m_VehicleSelect);
        GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("CharSelectScreen");
    }

    //change active view
    void GoToView ( GameObject newView ) {
        newView.SetActive(true);
        m_CurrentView.SetActive(false);
        m_CurrentView = newView;
    }

    void RefreshAllUI () {
        m_VehicleSelect.GetComponent<CharSelectController>().UpdateUI();
    }
    
    void WarnExit(){
        m_MessagePopup.m_Ok.AddListener (ExitGame);
        m_MessagePopup.OpenPopup(LocalizationText.GetText("exit_game_title_lbl"), LocalizationText.GetText("exit_game_body_lbl"));
    }
    
    void ExitGame(){
        Application.Quit(); 
    }
    
    void Update(){
        if ( Input.GetKeyDown(KeyCode.Escape) && !m_MessagePopup.m_Opened && !m_CoinsShop.m_Opened ) {
            if( m_CurrentView == m_HomePage ){
                WarnExit();
            }else{
                GoToView(m_HomePage);
            }
        }
    }
}
