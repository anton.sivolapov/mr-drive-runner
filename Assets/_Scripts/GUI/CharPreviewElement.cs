﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharPreviewElement : MonoBehaviour {
    public GameObject m_LockedState;
    public GameObject m_LockedStateAdditional;
    public GameObject m_OpenedState;
    public Image m_PreviewSprite;
    public Text m_VehicleLabel;
    public Text m_Price;
    public Text m_Control;
    public Text m_Speed;
    public Button m_BuyButton;
    public GameObject m_ActiveState;

    public Vehicle m_Vehicle;
    
    public bool m_Opened;
    public int m_VehicleIndex;

    // Use this for initialization
    void Start () {
        m_PreviewSprite.sprite = m_Vehicle.m_PreviewSpriteShop;
        m_VehicleLabel.text = m_Vehicle.m_Label;

        m_BuyButton.onClick.AddListener(BuyVehicle);

        m_Price.text = m_Vehicle.m_Price.ToString();
        m_Speed.text = '+' + m_Vehicle.m_AdditionalLandSpeed.ToString();
        m_Control.text = m_Vehicle.m_MovingSpeed.ToString();

        if( m_Opened ){
            m_LockedState.SetActive(false);
            m_LockedStateAdditional.SetActive(false);
            m_OpenedState.SetActive(true);
        }else{
            m_LockedState.SetActive(true);
            m_LockedStateAdditional.SetActive(true);
            m_OpenedState.SetActive(false);
        }
    }

    public void MakeInactive(){
        m_ActiveState.SetActive(false);
    }

    public void MakeActive(){
        if( !m_ActiveState.activeSelf ){
            m_ActiveState.SetActive(true);
            m_ActiveState.transform.localScale = Vector3.zero;
            iTween.ScaleTo(m_ActiveState, iTween.Hash("scale", Vector3.one, "easeType", "easeInOutQuad", "time", 0.1f ));
        }
    }
    
    public void MakeOpened(){
        m_Opened = true;
        m_LockedState.SetActive(false);
        m_LockedStateAdditional.SetActive(false);
        m_OpenedState.SetActive(true);
    }

    public void BuyVehicle(){
        gameObject.SendMessageUpwards("BuyVehicleFromStore", m_VehicleIndex);
    }
}
