﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsShopController : MonoBehaviour {

    public Button m_BackButton;
    public Text m_CoinsText;
    // Use this for initialization
    void Start () {
        m_BackButton.onClick.AddListener(GoBack);
        m_CoinsText.text = string.Format("{0:n0}", GlobalGameManager.instance.m_Coins);
    }
    
    // Update is called once per frame
    void GoBack () {
        gameObject.SendMessageUpwards("GoBackHome");
    }
}
