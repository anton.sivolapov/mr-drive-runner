﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharListController : MonoBehaviour {
    public int m_CurrentPage = 0;
    public int m_CharsPerPage = 8;
    public Button m_NextPageButton;
    public Button m_PrevPageButton;

    // Use this for initialization
    void Start () {
        m_NextPageButton.onClick.AddListener(NextPage);
        m_PrevPageButton.onClick.AddListener(PrevPage);

        ShowPage(m_CurrentPage);
    }
    
    void ShowPage ( int pageNum ) {
        int startPageNum = pageNum * m_CharsPerPage;
        int stopPageNum = startPageNum + m_CharsPerPage;
        int childNum = 0;
        foreach(Transform child in transform) {
            if( childNum >= startPageNum && childNum < stopPageNum ){
                child.gameObject.SetActive(true);
            }else{
                child.gameObject.SetActive(false);
            }
            childNum++;
        }
        
        if( pageNum == 0 ){
            m_PrevPageButton.gameObject.SetActive(false);
        }else{
            m_PrevPageButton.gameObject.SetActive(true);
        }
        if( childNum < stopPageNum ){
            m_NextPageButton.gameObject.SetActive(false);
        }else{
            m_NextPageButton.gameObject.SetActive(true);
        }
    }

    void NextPage () {
        ShowPage( ++m_CurrentPage );
    }

    void PrevPage () {
        ShowPage( --m_CurrentPage );
    }
    
    public void VehicleSelected( GameObject element ){
        foreach(Transform child in transform) {
            if( element != child.gameObject ){
                child.localScale = Vector3.one;
            }
        }
        MakeActive(element);
    }

    //mark vehicle active   
    public void MakeActive(GameObject el){
        if( el.transform.localScale == Vector3.one ){
            el.transform.localScale = el.transform.localScale * 1.2f;
        }else{
            el.transform.localScale = Vector3.one;
        }
    }
}
