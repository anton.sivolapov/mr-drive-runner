﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using UnityEngine.UI.Extensions;

public class CharSelectController : MonoBehaviour {

    public Button m_BackButton;
    public Button m_AddCoinsButton;
    public GameObject m_VehicleList;
    public GameObject m_CharListElement;
    public Transform m_PreviewsList;
    public MessagePopup m_Popup;
    public Text m_CoinsText;
    public HorizontalScrollSnap m_Scroll;

    private List<CharPreviewElement> m_ListOfPreviews = new List<CharPreviewElement>();
    private int m_IndexToOpen;
    private int m_ModalType;

    // Use this for initialization
    void Start () {
        m_BackButton.onClick.AddListener(GoBack);
        m_AddCoinsButton.onClick.AddListener(ShowShop);

        UpdateUI();
        InitPreviews();
    }

    void CenterCarousel(){
        int selectedIndex  = GlobalGameManager.instance.m_SelectedVehicleIndex;
        m_Scroll.GoToScreen(selectedIndex);
    }
    void OnEnable(){
        UpdateUI();
        Invoke("CenterCarousel", 0.1f);
    }

    public void UpdateUI(){
        m_CoinsText.text = string.Format("{0:n0}", GlobalGameManager.instance.m_Coins);
    }
    
    public void InitPreviews(){
        GameData gameData = GlobalGameManager.instance.m_GameData;
        bool[] opened  = GlobalGameManager.instance.m_OpenedVehicles;
        int selectedIndex  = GlobalGameManager.instance.m_SelectedVehicleIndex;
        int index = 0;
        foreach( Vehicle v in gameData.m_Vehicles ){
            // Debug.Log(m_CharListElement);
            // Debug.Log(v);
            GameObject previewEl = Instantiate( m_CharListElement );
            // Debug.Log(previewEl);
            // Debug.Log(m_Scroll);
            m_Scroll.AddChild(previewEl.gameObject);

            CharPreviewElement previewComponent = previewEl.GetComponentInChildren<CharPreviewElement>();

            bool isOpened = opened[index];
            previewComponent.m_Vehicle = v;
            previewComponent.m_Opened = isOpened;
            previewComponent.m_VehicleIndex = index;
            // if( isSelected ){
            // }else if( isOpened ){
            //     previewEl.MakeOpened();
            // }else{
            //     previewEl.MakeClosed();
            // }
            // // previewEl.m_Selected = isSelected;
            
            m_ListOfPreviews.Add(previewComponent);
            
            index++;
        }
    }

    public void InitPreviewsOld(){
        GameData gameData = GlobalGameManager.instance.m_GameData;
        bool[] opened  = GlobalGameManager.instance.m_OpenedVehicles;
        int selectedIndex  = GlobalGameManager.instance.m_SelectedVehicleIndex;
        // m_CurrentScore.text = GlobalGameManager.instance.m_CurrentScore.ToString();

        float lastPreviewPos = 0f;
        int index = 0;
        foreach( Vehicle v in gameData.m_Vehicles ){
            GameObject preview = Instantiate( v.m_Preview );
            Vector3 origPos = preview.transform.position;
            //move preview little bit, to avoid ovelaping
            origPos.x = lastPreviewPos + 100f;
            preview.transform.position = origPos;

            //update last preview position
            lastPreviewPos = preview.transform.position.x;

            preview.transform.parent = m_PreviewsList;
            Camera camera = preview.GetComponentInChildren<Camera>();

            GameObject charElement = Instantiate( m_CharListElement );
            charElement.transform.SetParent(m_VehicleList.transform);
            charElement.transform.localScale = Vector3.one;

            charElement.GetComponentInChildren<RawImage>().texture = camera.targetTexture;
            
            bool isOpened = opened[index];
            bool isSelected = index == selectedIndex;
            CharPreviewElement previewEl = charElement.GetComponent<CharPreviewElement>();
            previewEl.m_Vehicle = v;
            previewEl.m_Opened = isOpened;
            if( isSelected ){
            }else if( isOpened ){
                previewEl.MakeOpened();
            }else{
                // previewEl.MakeClosed();
            }
            // previewEl.m_Selected = isSelected;
            previewEl.m_VehicleIndex = index;
            
            m_ListOfPreviews.Add(previewEl);
            
            index++;
        }
        SelectVehicle(selectedIndex);
    }
    
    // Update is called once per frame
    void GoBack () {
        gameObject.SendMessageUpwards("GoBackHome");
    }
    
    void ShowShop () {
        Debug.Log("Go to shop");
        gameObject.SendMessageUpwards("GoToCoinsShop");
    }
    
    void SelectVehicle( int index ){
        GlobalGameManager.instance.m_SelectedVehicleIndex = index;

        int i = 0;
        foreach( CharPreviewElement v in m_ListOfPreviews ){
            // v.m_Selected = false;
            if( index == i ){
                // v.MakeSelected();
                // m_PlayerPreview.texture = v.GetTexture();
            }else if( v.m_Opened ){
                v.MakeOpened();
            }
            i++;
        }
    }

    void OnOk (){
        if( m_ModalType == 1 ){
            GlobalGameManager.instance.OpenVehicle(m_IndexToOpen);
            m_ListOfPreviews[m_IndexToOpen].MakeOpened();

            SelectVehicle( m_IndexToOpen );
            m_IndexToOpen = 0;

            UpdateUI();
        }else if( m_ModalType == 2 ){
            gameObject.SendMessageUpwards("GoToCoinsShop");
        }

        m_Popup.m_Ok.RemoveListener (OnOk);
        m_Popup.m_Cancel.RemoveListener (OnCancel);
    }

    void OnCancel (){
        if( m_ModalType == 1 ){
            m_IndexToOpen = 0;
        }else if( m_ModalType == 2 ){
            //do nothing
        }

        m_Popup.m_Ok.RemoveListener (OnOk);
        m_Popup.m_Cancel.RemoveListener (OnCancel);
    }

    void BuyVehicleFromStore( int index ){
        GameData gameData = GlobalGameManager.instance.m_GameData;
        m_IndexToOpen = index;
        m_ModalType = 1;
        

        m_Popup.m_Ok.AddListener (OnOk);
        m_Popup.m_Cancel.AddListener (OnCancel);

        if( GlobalGameManager.instance.m_Coins >= gameData.m_Vehicles[index].m_Price ){
            m_Popup.OpenPopup(LocalizationText.GetText("open_vehicle_confirmation_title"), LocalizationText.GetText("open_vehicle_confirmation_body"));
        }else{
            m_ModalType = 2;
            m_Popup.OpenPopup(LocalizationText.GetText("open_vehicle_low_coins_title"), LocalizationText.GetText("open_vehicle_low_coins_body"));
        }
    }

    public void SelectionChaged(){
        Debug.Log("SelectionChaged");
        foreach( CharPreviewElement el in m_ListOfPreviews ){
            if( el.m_VehicleIndex != m_Scroll.CurrentPage )
                el.MakeInactive();
        }
        m_ListOfPreviews[m_Scroll.CurrentPage].MakeActive();

        bool[] opened  = GlobalGameManager.instance.m_OpenedVehicles;
        if( opened[m_Scroll.CurrentPage] ){
            GlobalGameManager.instance.m_SelectedVehicleIndex = m_Scroll.CurrentPage;
        }
    }

    // public void SelectionChagedStart(int i){
    //     // Debug.Log(i);
    //     // Debug.Log(m_Scroll.CurrentPage);
    // }

    // public void SelectionChagedEnd( int i ){
    //     Debug.Log(i);
    //     Debug.Log(m_Scroll.CurrentPage);
    // }

    // void Update(){
    //     Debug.Log(m_Scroll.CurrentPage);
    //     int i = 0;
    //     foreach( Transform t in m_Scroll.transform.GetChild(0).transform ){
    //         if( i == m_Scroll.CurrentPage ){
    //             t.transform.localScale = Vector3.one;
    //         }else{
    //             t.transform.localScale = new Vector3( 0.5f, 0.5f, 0.5f );
    //         }
    //         i++;
    //     }
    // }
}
