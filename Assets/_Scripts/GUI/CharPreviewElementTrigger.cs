﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharPreviewElementTrigger : EventTrigger {
    public override void OnPointerClick( PointerEventData data )
    {
        gameObject.SendMessageUpwards("VehicleSelected", gameObject);
    }
}
