﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickSound : MonoBehaviour {
    public AudioClip m_AudioClip;
    
    private AudioSource m_AudioSource;
    // Use this for initialization
    void OnEnable () {
        m_AudioSource = GetComponent<AudioSource>();
        if( m_AudioSource == null ){
            m_AudioSource = gameObject.AddComponent<AudioSource>();
        }

        GetComponent<Button>().onClick.AddListener(PlayClick);
    }
    
    void PlayClick(){
        m_AudioSource.PlayOneShot(m_AudioClip);
    }
}
