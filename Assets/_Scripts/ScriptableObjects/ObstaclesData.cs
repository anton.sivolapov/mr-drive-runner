﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//represents list of obstacles it useful to set obstacles for one level 
//by theme
[System.Serializable]
public class Obstacle {
    public GameObject m_Prefab;

    [Tooltip("Total number width in units")]
    public int m_Width = 1;
    // [Tooltip("Number of clear space in units")]
    // public int m_WindowSize = 0;
    public string m_StringCode;
}

[CreateAssetMenu(menuName = "Create Obstacles List")]
public class ObstaclesData : ScriptableObject {
    [Header("Obstacles")]
    [Tooltip("List of obstacles. All measures in units ( one unit will be calculated on fly ).")]
    public Obstacle[] m_Items;
}
