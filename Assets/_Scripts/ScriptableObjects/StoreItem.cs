﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Create Store Item")]
public class StoreItem : ScriptableObject {
    public bool m_NonConsumable;
    public float m_Price;
    public string m_ProductId;
    public int m_CoinsNum;
    public int m_PotionsNum;
    public GameObject m_PreviewPrefab;
}
