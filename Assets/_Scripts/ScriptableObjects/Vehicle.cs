﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Create Vehicle Data")]
public class Vehicle : ScriptableObject {
    public GameObject m_Renderer;
    public GameObject m_Preview;
    public Sprite m_PreviewSprite;
    public Sprite m_PreviewSpriteShop;
    public string m_Label;

    public float m_MovingSpeed = 30f;
    public int m_AdditionalScore = 1;
    public int m_AdditionalCoins = 1;
    public int m_AdditionalLandSpeed = 1;
    public string m_SpecialAbilityName;
    
    public int m_Price = 100;
    public int m_Health = 1;

    [Header("Themes")]
    public LevelTheme[] m_PossibleLevelThemes;
    public GameObject[] m_Animals;
}
