﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//difficulty level
[System.Serializable]
public class ObstacleLevel {
    [Tooltip("Total number width in units")]
    public int m_WindowSize = 1;
    [Tooltip("Number of clear space in units")]
    public int m_NumberOfWindows = 1;
    public int m_MinimumWinSize = 2;
}


[CreateAssetMenu(menuName = "Create Game Data")]
public class GameData : ScriptableObject {
    public int m_InitScore = 0;
    public int m_InitCoins = 500;
    public int m_InitPotions = 2;
    public float m_BaseLandSpinSpeed = 10f;
    public Transform m_OneUnitObstacle;

    [Header("Leaderboards Android")]
    public string m_GlobalLeaderboardIdAndroid = "top_players_score_leaderboard";
    public string m_GlobalDistanceLeaderboardIdAndroid = "top_players_distance_leaderboard";
    public string m_GlobalMultiplayerLeaderboardIdAndroid = "top_players_multiplayer_leaderboard";

    [Header("Leaderboards iOS")]
    public string m_GlobalLeaderboardIdIOS = "top_players_score_leaderboard";
    public string m_GlobalDistanceLeaderboardIdIOS = "top_players_distance_leaderboard";
    public string m_GlobalMultiplayerLeaderboardIdIOS = "top_players_multiplayer_leaderboard";

    [Header("Collectables")]
    public ObstaclesData m_PowerUps;
    public ObstaclesData m_Coins;
    public ObstaclesData m_Rockets;

    [Header("Playable Characters")]
    public Vehicle[] m_Vehicles;

    [Header("Obstacles Levels")]
    public ObstacleLevel[] m_ObstacleLevels;

    [Header("Game Goods")]
    public StoreItem[] m_GameGoods;
}
