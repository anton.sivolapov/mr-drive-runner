﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Level Theme")]
[System.Serializable]
public class LevelTheme : ScriptableObject{
    public GameObject m_CenterPrefab;
    public Material m_LandMaterial;
    public Material m_LandBottomMaterial;
    public ObstaclesData m_SmallObstacles;
    public ObstaclesData m_MediumObstacles;
    public ObstaclesData m_BigObstacles;
    public ObstaclesData m_Animals;
    public Sprite m_Sky;
}
