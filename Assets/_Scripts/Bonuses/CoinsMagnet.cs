﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsMagnet : MonoBehaviour {
    void OnTriggerEnter( Collider other ){
        if( other.gameObject.tag == "Collectable" && other.gameObject.GetComponentInParent<CoinController>() != null ){
            StartCoroutine(PickCollectable(other.gameObject));
        }
    }

    IEnumerator PickCollectable( GameObject collectable ){
        Vector3 startPos = collectable.transform.position;
        float startTime = Time.time;
        float speed = 0.3f;
        while( collectable != null ){
            float distCovered = (Time.time - startTime) / speed;
            collectable.transform.position = Vector3.Slerp(startPos, transform.position, distCovered);
            yield return null;
        }
    }
}
