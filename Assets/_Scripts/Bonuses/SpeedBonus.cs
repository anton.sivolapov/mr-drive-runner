﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBonus : PowerUp {
    public float m_SpeedBonus = 40f;
    public int m_Coins = 20;

    DifficultyManager m_DifficultyManager;
    float m_OriginalSpeed;

    // Use this for initialization
    void Start () {
        m_DifficultyManager = FindObjectOfType<DifficultyManager>();
    }

    public void Activate(){
        m_DifficultyManager.TmpSpeedBonus();
    }

    IEnumerator Cooldown(){
        yield return new WaitForSeconds( 1f );
        m_DifficultyManager.m_AdditionalLandSpeed = 0;
    }
    
    public override void Apply( MovementController mc ){
        Activate();
        mc.OnCoinsCollected(m_Coins);
    }
}
