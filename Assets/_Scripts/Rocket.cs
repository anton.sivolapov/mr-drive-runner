﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour {
    public Transform m_AnimalPosition1;
    public Transform m_AnimalPosition2;
    public Transform m_AnimalPosition3;

    public Vector3 m_Velocity;

    [HideInInspector]
    public GameObject m_Animal1;
    [HideInInspector]
    public GameObject m_Animal2;
    [HideInInspector]
    public GameObject m_Animal3;

    Rigidbody m_Rigidbody;
    Transform m_FlightParent;

    void Awake(){
        m_Rigidbody = GetComponent<Rigidbody>();
        m_FlightParent = GameObject.FindObjectOfType<GameController>().gameObject.transform;
    }

    void OnTriggerEnter(Collider other) {
        if( other.gameObject.tag == "Player" ){
            other.gameObject.GetComponentInParent<MovementController>().OnRocketCollected(this);

            transform.SetParent( m_FlightParent );
            m_Rigidbody.velocity = m_Velocity;

            Invoke("DestroySelf", 5f);
        }
    }

    void DestroySelf(){
        Destroy(gameObject);
    }
}
