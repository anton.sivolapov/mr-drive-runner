﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class ShadowProjector : MonoBehaviour {
    public GameObject m_ProjectorPrefab;

    private Projector m_Projector;
    // Use this for initialization
    void Start () {
        GameObject projector = Instantiate(m_ProjectorPrefab, transform);

        // Vector3 newPos = Vector3.zero;
        // newPos.z += 1;
        // projector.transform.position = newPos;

        // projector.transform.SetParent(transform);

        Centerer centerer = GetComponent<Centerer>();

        m_Projector = projector.GetComponent<Projector>();
        if( centerer != null ){
            m_Projector.orthographicSize = Math.Max(Math.Abs(centerer.m_Center.x), Math.Abs(centerer.m_Center.x)) * 2;
        }else{
            m_Projector.orthographicSize = transform.localScale.x * 1.5f;
        }
        m_Projector.ignoreLayers = 1 << gameObject.layer;
    }
    
    // Update is called once per frame
    void Update () {
        
    }
}
