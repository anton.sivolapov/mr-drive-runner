﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingLayer : MonoBehaviour {
    public string m_LayerName;
    public int m_Order;
    public Renderer m_Renderer;
    // Use this for initialization
    void Start () {
        Renderer r = m_Renderer;
        r.sortingLayerName = m_LayerName;
        r.sortingOrder = m_Order;	
    }
}
