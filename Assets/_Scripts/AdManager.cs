﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour {
    public RewardBasedVideoAd m_RewardBasedVideoAd;
    private InterstitialAd m_Interstitial;

    private Action m_RewardCallback;
    private Action m_RewardFallback;

    // Use this for initialization
    void Start () {
        m_RewardBasedVideoAd = RewardBasedVideoAd.Instance;
        m_RewardBasedVideoAd.OnAdRewarded += HandleOnAdRewarded;
        m_RewardBasedVideoAd.OnAdClosed += HandleOnAdClosed;

        LoadRewardBasedAd();
        LoadInterstitialAd();
    }

    AdRequest BuildAdRequest(){
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        //test

        // AdRequest request = new AdRequest.Builder()
        //     .AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
        //     .AddTestDevice("DFC6FBAC6FC4D473BF035ECF93959EDC")  // My test device.
        //     .Build();

        //end test

        return request;
    }

    void LoadInterstitialAd(){
        #if UNITY_ANDROID
        string adUnitId = "ca-app-pub-5316161378481999/3773547468";
        #elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-5316161378481999/5250280668";
        #else
        string adUnitId = "unexpected_platform";
        #endif
        m_Interstitial = new InterstitialAd(adUnitId);

        m_Interstitial.LoadAd(BuildAdRequest());
        m_Interstitial.OnAdClosed += ReloadInterstitialAd;
    }

    public void ReloadInterstitialAd(  object sender, System.EventArgs args ){
        LoadInterstitialAd();
    }

    public void ShowInterstitialAd(){
        if( m_Interstitial.IsLoaded() ){
            m_Interstitial.Show();
        }
    }

    void LoadRewardBasedAd(){
        #if UNITY_ANDROID
        string adUnitId = "ca-app-pub-5316161378481999/9820081069";
        //test
        // string adUnitId = "ca-app-pub-3940256099942544/5224354917";
        #elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-5316161378481999/2296814268";
        #else
        string adUnitId = "unexpected_platform";
        #endif


        // Load the interstitial with the request.
        m_RewardBasedVideoAd.LoadAd(BuildAdRequest(), adUnitId);
    }

    public void ShowRewardBasedAd(Action cb, Action fb){
        m_RewardCallback = cb;
        m_RewardFallback = fb;

        if( m_RewardBasedVideoAd.IsLoaded() ){
            m_RewardBasedVideoAd.Show();
        } else {
            Debug.Log("Don't have an ad yet.");
        }
    }

    // public void HandleOnAdRewardedText(){
    //     if( m_RewardCallback != null ){
    //         m_RewardCallback();
    //     }
    // }

    public void HandleOnAdRewarded( object sender, System.EventArgs args ){
        Debug.Log("You got a reward");
        if( m_RewardCallback != null ){
            m_RewardCallback();
            m_RewardCallback = null;
            m_RewardFallback = null;
        }
        LoadRewardBasedAd();
    }

    public void HandleOnAdClosed( object sender, System.EventArgs args ){
        Debug.Log("Ad closed");
        if( m_RewardCallback != null ){
            m_RewardFallback();
            m_RewardCallback = null;
            m_RewardFallback = null;
        }
        LoadRewardBasedAd();
    }

    // public event EventHandler<EventArgs> OnAdLoaded = delegate { };
    // public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad = delegate { };
    // public event EventHandler<EventArgs> OnAdOpening = delegate { };
    // public event EventHandler<EventArgs> OnAdStarted = delegate { };
    // public event EventHandler<EventArgs> OnAdClosed = delegate { };
    // public event EventHandler<Reward> OnAdRewarded = delegate { };
    // public event EventHandler<EventArgs> OnAdLeavingApplication = delegate { };
}
