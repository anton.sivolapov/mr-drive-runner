﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialAbilityButton : MonoBehaviour {
    private GameController m_GameController;
    // Use this for initialization
    void Start () {
        m_GameController = FindObjectOfType<GameController>();

        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(ActivateAbility);
    }
    
    // Update is called once per frame
    void ActivateAbility () {
        m_GameController.m_Player1.m_Special.Activate();
        if( m_GameController.m_Player2 != null ){
            m_GameController.m_Player2.m_Special.Activate();
        }
    }
}
