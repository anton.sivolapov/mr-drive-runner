﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Analytics;

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener
{
    public static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    private GameShop m_GameShop;
        
    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    // public static string kProductIDConsumable =    "starter_pack";   
         
    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }
        
    public void InitializePurchasing() 
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }
            
        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            
        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        foreach( StoreItem si in GlobalGameManager.instance.m_GameData.m_GameGoods ){
            if( si.m_NonConsumable ){
                builder.AddProduct(si.m_ProductId, ProductType.NonConsumable);
            }else{
                builder.AddProduct(si.m_ProductId, ProductType.Consumable);
            }
        }
        // Continue adding the non-consumable product.
        // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
        // if the Product ID was configured differently between Apple and Google stores. Also note that
        // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
        // must only be referenced here. 
            
        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }
        
        
    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }
        
        
    public void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);
                
            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
        
    //  
    // --- IStoreListener
    //
        
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");
            
        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        // m_StoreProducts = controller.products;
        // Debug.Log("========================");
        // foreach (var item in controller.products.all)
        // {
        //     if (item.availableToPurchase)
        //     {
        //         Debug.Log(string.Join(" - ",
        //                               new[]
        //                                   {
        //                                       item.metadata.localizedTitle,
        //                                       item.metadata.localizedDescription,
        //                                       item.metadata.isoCurrencyCode,
        //                                       item.metadata.localizedPrice.ToString(),
        //                                       item.metadata.localizedPriceString
        //                                   }));
        //     }
        // }
    }

        
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
        
        
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
    {

        m_GameShop = FindObjectOfType<GameShop>();
        bool success = false;
        foreach( StoreItem si in GlobalGameManager.instance.m_GameData.m_GameGoods ){
            if( args.purchasedProduct.definition.id == si.m_ProductId ){
                success = true;
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));


                Analytics.CustomEvent("itemPurchased", new Dictionary<string, object>
                                      {
                                          { "product_id", args.purchasedProduct.definition.id }
                                      });

                string transactionID = string.Format("{0}_{1}", GlobalGameManager.instance.m_InstallationID, GlobalGameManager.instance.m_TransactionCounter++);

                GlobalGameManager.instance.m_GoogleAnalytics.LogTransaction(transactionID,
 "Coins Shop", si.m_Price, 0.0, 0.0, "USD" );

                GlobalGameManager.instance.m_GoogleAnalytics.LogItem(transactionID,
 si.m_ProductId, si.m_ProductId, "coins", si.m_Price, 1 );
                
                GlobalGameManager.instance.OnProductPurchased(args.purchasedProduct.definition.id, si);
                break;
            }
        }

        if( !success ){
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }
        
        
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public void RestorePurchase(){
        m_StoreExtensionProvider.GetExtension<IAppleExtensions> ().RestoreTransactions (result => {
                if (result) {
                    // This does not mean anything was restored,
                    // merely that the restoration process succeeded.
                    Debug.Log("Restoration SUCCESS");
                } else {
                    // Restoration failed.
                    Debug.Log("Restoration FAILED");
                }
            });

    }
}
