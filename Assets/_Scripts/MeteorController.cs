﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorController : MonoBehaviour {
    public GameObject m_MeteorPrefab;
    public Vector3 m_Velocity;

    // Use this for initialization
    void Start () {
            // ThrowMeteor();
        StartCoroutine(ThrowMeteors());
    }
    
    IEnumerator ThrowMeteors(){
        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }

            yield return new WaitForSeconds(Random.Range(2f, 10f));

            ThrowMeteor();
        }
    }

    void ThrowMeteor(){
        GameObject meteor = Instantiate( m_MeteorPrefab, transform );

        // Meteor m = m_MeteorPrefab.GetComponent<Meteor>()
        //     .GetPooledInstance<Meteor>();
        // GameObject meteor = m.gameObject;

        // meteor.transform.SetParent(transform);
        // meteor.transform.localPosition = Vector3.zero;
        // meteor.transform.position = transform.position;

        Rigidbody rigidbody = meteor.GetComponent<Rigidbody>();
        rigidbody.velocity = m_Velocity;
    }
}
