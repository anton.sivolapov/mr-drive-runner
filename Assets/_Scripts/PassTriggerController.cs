﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassTriggerController : MonoBehaviour {
    public ObstaclesSpawner m_ObstacleSpawner;

    void OnTriggerEnter(Collider other){
        if( other.gameObject.tag == "Player" ){
            m_ObstacleSpawner.LinePassed();
            Destroy(this.gameObject);
        }
    }
}
