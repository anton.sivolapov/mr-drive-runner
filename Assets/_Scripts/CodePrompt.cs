﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CodePrompt : MonoBehaviour {
    public Button m_OkButton;
    public Button m_CancelButton;
    public InputField m_InputField;
    
    public UnityEvent m_Ok;
    public UnityEvent m_Cancel;

    void Awake () {
        m_OkButton.onClick.AddListener(Ok);
        m_CancelButton.onClick.AddListener(Cancel);
    }

    public void ClosePopup(){
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Ok () {
        string val = m_InputField.text;

        if( val == "fps" ){
            GlobalGameManager.instance.ToggleFPS();
            GlobalGameManager.instance.Notify("FPS displayed");
        }else if( val == "pool" ){
            GlobalGameManager.instance.m_UseObjectPool = true;
            GlobalGameManager.instance.Notify("Pool enabled");
        }else if( val == "unpool" ){
            GlobalGameManager.instance.m_UseObjectPool = false;
            GlobalGameManager.instance.Notify("Pool disabled");
        }else{
            GlobalGameManager.instance.Notify("Wrong code");
        }
        ClosePopup();
    }

    // Update is called once per frame
    void Cancel () {
        ClosePopup();
    }
}
