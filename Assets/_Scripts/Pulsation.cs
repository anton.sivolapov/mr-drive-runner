﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsation : MonoBehaviour {
    public float m_PulsationTime = 0.1f;
    public float m_PulsationScale = 1.05f;
    public float m_Delay = 0f;

    public void StartPulsation(){
        Vector3 targetScale = transform.localScale * m_PulsationScale;
        iTween.ScaleTo(gameObject, iTween.Hash("scale", targetScale, "easeType", "easeInOutQuad", "time", m_PulsationTime, "loopType", "pingPong" ));
    }

    public void StopPulsation(){
        iTween.Stop(gameObject);

    }
    // Use this for initialization
    void Start () {
        if( m_Delay != 0f ){
            Invoke("StartPulsation", m_Delay);
        }else{
            StartPulsation();
        }
    }
}
