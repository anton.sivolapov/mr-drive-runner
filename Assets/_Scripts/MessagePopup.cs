﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MessagePopup : MonoBehaviour {
    public Text m_Title;
    public Text m_BodyText;
    public Button m_OkButton;
    public Button m_OkButton2;
    public Button m_CancelButton;
    public Button m_CodeButton;
    public GameObject m_PromptPopup;
    public GameObject m_TwoButtonsLayout;
    public GameObject m_OneButtonLayout;
    
    public UnityEvent m_Ok;
    public UnityEvent m_Cancel;


    public bool m_Opened;

    public MessagePopup(){
        m_Ok = new UnityEvent();
        m_Cancel = new UnityEvent();
    }

    void Awake () {
        m_OkButton.onClick.AddListener(Ok);
        m_OkButton2.onClick.AddListener(Ok);
        m_CancelButton.onClick.AddListener(Cancel);
        // m_CodeButton.onClick.AddListener(ShowCodePrompt);
    }

    // Use this for initialization
    void Start () {
    }

    public void ShowCodePrompt(){
        m_PromptPopup.SetActive(true);
    }

    public void OpenPopupWithCode( string title, string body ){
        OpenPopup(title, body);
        // m_CodeButton.gameObject.SetActive(true);
    }

    public void OpenPopup( string title, string body, bool oneBtn = false ){
        m_Opened = true;
        if( oneBtn ){
            m_TwoButtonsLayout.SetActive(false);
            m_OneButtonLayout.SetActive(true);
        }else{
            m_TwoButtonsLayout.SetActive(true);
            m_OneButtonLayout.SetActive(false);
        }

        gameObject.SetActive(true);
        m_Title.text = title;
        m_BodyText.text = body;
        m_PromptPopup.SetActive(false);
        // m_CodeButton.gameObject.SetActive(false);
    }

    public void ClosePopup(){
        m_Opened = false;

        m_Ok.RemoveAllListeners();
        m_Cancel.RemoveAllListeners();

        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Ok () {
        m_Ok.Invoke();
        ClosePopup();
    }

    // Update is called once per frame
    void Cancel () {
        m_Cancel.Invoke();
        ClosePopup();
    }
    
    void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)) {
            ClosePopup();
        }
    }
}
