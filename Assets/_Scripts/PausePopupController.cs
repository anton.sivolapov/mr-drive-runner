﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PausePopupController : MonoBehaviour {
    public Button m_HomeButton;
    public Button m_ContinueButton;
    public Button m_ResetButton;
    public GameShop m_GameShop;

    void Start () {
        m_HomeButton.onClick.AddListener(OnHomeClick);
        m_ResetButton.onClick.AddListener(OnResetClick);
        m_ContinueButton.onClick.AddListener(OnContinueClick);
    }
    
    void OnResetClick () {
        LoadingScreenManager.LoadSceneWithLoadingInfo(1);
    }

    void OnHomeClick () {
        gameObject.SendMessageUpwards("GoHomeScreen");
    }

    void OnContinueClick () {
        gameObject.SendMessageUpwards("ResumeGame");
    }
}
