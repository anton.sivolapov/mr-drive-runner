﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : DifficultyManager {
    public tutState m_TutorialState;
    public enum tutState { Move, Acceleration, Animals, Obstacle, Rocket };
    public Text m_TutorialText;

    // Coroutine currentStateHandler;
    

    private int m_NumCoins;
    private int m_NumAnimals;
    private int m_NumRockets;
    private int m_SpecialAbility;

    void Start(){
        m_TutorialState = tutState.Move;
        base.InitGame();
        StartCoroutine(Tutorial());
    }

    IEnumerator Tutorial(){
        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(MoveTutorial());
        yield return StartCoroutine(Acceleration());
        yield return StartCoroutine(Animals());

        yield return null;
    }

    IEnumerator MoveTutorial(){
        //show desc
        m_GameController.PauseGame();

        m_GameController.m_MessagePopup.m_Ok.AddListener (UnpauseGame);
        m_GameController.m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedtutorial_lbl"), LocalizationText.GetText("movetutorial_lbl"), true);


        //place coins
        while( m_NumCoins < 2 ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            yield return new WaitForSeconds(2f);
            m_ObstacleSpawner.PlaceCoins( 2 );
        }
        //detect moves
        m_TutorialText.text = LocalizationText.GetText("donetutorial_lbl");
        //end part
        yield return new WaitForSeconds(2f);
    }

    void UnpauseGame(){
        m_GameController.m_MessagePopup.m_Ok.RemoveListener (UnpauseGame);
        m_GameController.ResumeGame();
    }

    IEnumerator Acceleration(){
        m_TutorialText.text = "";
        m_GameController.PauseGame();

        m_GameController.m_MessagePopup.m_Ok.AddListener (UnpauseGame);
        m_GameController.m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedtutorial_lbl"), LocalizationText.GetText("accelerationtutorial_lbl"), true);


        while( m_SpecialAbility < 1 ){
            yield return null;
        }
        m_TutorialText.text = LocalizationText.GetText("donetutorial_lbl");
        yield return new WaitForSeconds(2f);
        yield return null;
    }

    IEnumerator Animals(){
        m_TutorialText.text = "";
        //show desc
        m_GameController.PauseGame();

        m_GameController.m_MessagePopup.m_Ok.AddListener (UnpauseGame);
        m_GameController.m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedtutorial_lbl"), LocalizationText.GetText("animaltutorial_lbl"), true);
        //place coins
        while( m_NumAnimals < 1 ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            yield return new WaitForSeconds(2f);
            m_ObstacleSpawner.PlaceAnimals( 1 );
        }
        //detect moves
        m_GameController.PauseGame();

        m_GameController.m_MessagePopup.m_Ok.AddListener (UnpauseGame);
        m_GameController.m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedtutorial_lbl"), LocalizationText.GetText("rockettutorial_lbl"), true);

        while( m_NumRockets < 1 ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            yield return new WaitForSeconds(2f);
            m_ObstacleSpawner.PlaceRockets( 1 );
        }
        //end part
        m_GameController.PauseGame();

        m_GameController.m_MessagePopup.m_Ok.AddListener (UnpauseGame);
        m_GameController.m_MessagePopup.OpenPopup(LocalizationText.GetText("proceedtutorial_lbl"), LocalizationText.GetText("finishtutorial_lbl"), true);

        while( GlobalGameManager.paused ){
            yield return null;
        }
        yield return new WaitForSeconds(2f);

        LoadingScreenManager.LoadSceneWithLoadingInfo(1);
    }

    public override void OnObstacleTriggerEnter(){
        // Debug.Log("do nothing");
    }

    public override void OnCoinCollected(){
        m_NumCoins++;
    }

    public override void OnSpecialAbilityActivated(){
        m_SpecialAbility++;
    }

    public override void OnAnimalCollected(){
        m_NumAnimals++;
    }

    public override void OnRocketCollected(){
        m_NumRockets++;
    }
}
