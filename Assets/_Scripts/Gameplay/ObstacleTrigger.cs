﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTrigger : MonoBehaviour {
    private DifficultyManager m_DifficultyManager;
    void Awake(){
        m_DifficultyManager = FindObjectOfType<DifficultyManager>();
    }

    void OnTriggerEnter( Collider collider ){
        if( collider.gameObject.tag == "Player" ){
            if( GlobalGameManager.instance.m_IsMultiplayerGame ){
                MovementController mc = collider.gameObject.GetComponentInParent<MovementController>() ;
                if( mc != null && mc.m_PlayerNum == 1 ){
                    m_DifficultyManager.OnObstacleTriggerEnter();
                }
            }else{
                m_DifficultyManager.OnObstacleTriggerEnter();
            }
        }
    }
}
