﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleShreder : MonoBehaviour {
    void OnTriggerEnter(Collider other){
        if( other.gameObject.tag == "Obstacle" || other.gameObject.tag == "Collectable"){
            // PooledObject po = other.GetComponentInParent<PooledObject>();
            Animal ac = other.GetComponentInParent<Animal>();
            if( ac != null ){
                GlobalGameManager.instance.IncNonSavedAnimal(ac.m_AnimalName);
            }
            // if( po != null ){
            //     po.ReturnToPool();
            // }else{
                Removable oc = other.gameObject.GetComponentInParent<Removable>();
                if( oc != null )
                    Destroy(oc.gameObject);
                else
                    Destroy(other.gameObject);
            // }
        }
    }
}
