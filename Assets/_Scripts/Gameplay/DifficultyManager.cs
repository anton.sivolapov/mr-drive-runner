﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DifficultyManager : MonoBehaviour {
    public ObstacleSpawnerV2 m_ObstacleSpawner;
    public bool m_HideWaypoints;
    
    [Header("Timers")]
    public float m_DifficultyIncreaseTimeMin = 90;
    public float m_DifficultyIncreaseTimeMax = 90;

    [Header("Waypoints")]
    public GameObject m_LastWaypoint;
    public GameObject m_AverageWaypoint;
    public GameObject m_HighWaypoint;

    [Header("Land Configuration")]
    public bool m_EnableLandSpin = true;
    public bool m_EnableObstacles = true;
    public Transform m_LandPieTop;
    public Transform m_TopLandAdditions;

    public Transform m_LandPieBottom;

    public float m_LandSpeed = 40;
    public float m_ObstacleSpawnInterval = 3f;
    public float m_AdditionalLandSpeed = 0;
    public float m_AdditionalLandSpeed1 = 0;
    public float m_LandSpeedBottom = 20;
    public float m_SpeedIncreaseStep = 10;

    [Header("Obstacles Configuration")]
    public int m_SmallObstacles = 1;
    public int m_MediumObstacles = 0;
    public int m_BigObstacles = 0;

    public CameraFilterPack_Blur_Radial_Fast m_Filter;

    // private string[] m_Components = new string[4] { "landSpeed", "smallObstacles", "mediumObstacles", "bigObstacles" };
    private int m_TriggersPassed;
    private int m_NumIncreased;
    private Vehicle m_Vehicle;
    private bool m_ObstaclesEnabled = true;
    protected GameController m_GameController;

    protected IEnumerator m_CurrentSpawnProcess;

    void Start(){
        InitGame();
    }

    public void InitGame(){
        m_Vehicle = GlobalGameManager.instance.m_SelectedVehicle;
        m_GameController = GetComponentInParent<GameController>();

        DisableObstaclesForSeconds(3f);

        StartCoroutine("IncreaseDifficultyTimer");
        StartCoroutine("RotateWorld");

        if( m_EnableObstacles ){
            StartCoroutine( SpawnObstacles() );
        }
    }

    IEnumerator SpawnObstacles(){
        while(true){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            yield return StartCoroutine( SpawnOnPlayerWay() );
        }
    }

    IEnumerator SpawnOnPlayerWay(){
        yield return new WaitForSeconds(m_ObstacleSpawnInterval);
        m_ObstacleSpawner.PlaceRandomBlockOnPlayerWay();

        yield return new WaitForSeconds( Random.Range(0.5f, m_ObstacleSpawnInterval/2));
        m_ObstacleSpawner.PlaceRandomBlockOnRandomPosition( m_SmallObstacles, 0, 0 );
    }

    IEnumerator IncreaseDifficultyTimer(){
        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            float wait = Random.Range( m_DifficultyIncreaseTimeMin, m_DifficultyIncreaseTimeMax );
            // float wait = 10f;
            yield return new WaitForSeconds(wait);
            m_NumIncreased++;

            //two times increase and then one time decrease difficulty
            if( m_NumIncreased % 3 != 0 ){
                IncreaseDifficulty( 1 );
            }else{
                DecreasedDifficulty( 1 );
            }
            // IncreaseDifficulty(1);
        }
    }
    
    IEnumerator RotateWorld(){
        float currentTop = GlobalGameManager.instance.m_TopDistance;
        float averageDistance = GlobalGameManager.instance.m_TotalDistance / GlobalGameManager.instance.m_NumberOfGameSessions;
        float lastDistance = m_GameController.m_LastDistance;
        
        bool lastEnabled = lastDistance > 25;
        bool averageEnabled = averageDistance > 50;
        bool highEnabled = currentTop > 70;

        if( Mathf.Abs(currentTop - averageDistance) < 50 ){
            averageEnabled = false;
        }
        if( Mathf.Abs(currentTop - lastDistance) < 50 ){
            lastEnabled = false;
        }
        if( Mathf.Abs(lastDistance - averageDistance) < 50 ){
            averageEnabled = false;
        }

        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            if( m_EnableLandSpin ){
                Quaternion deltaBottomRotation = Quaternion.Euler(new Vector3( 0f, m_LandSpeedBottom, 0f ) * Time.deltaTime);
                m_LandPieBottom.rotation = m_LandPieBottom.rotation * deltaBottomRotation;

                float additionalSpeed = m_AdditionalLandSpeed + m_AdditionalLandSpeed1;
                float totalSpeed = m_LandSpeed + m_Vehicle.m_AdditionalLandSpeed + additionalSpeed;
                m_GameController.OnWorldRotated(totalSpeed);

                Quaternion deltaRotation = Quaternion.Euler(new Vector3( 0f, -totalSpeed, 0f ) * Time.deltaTime);
                m_LandPieTop.rotation = m_LandPieTop.rotation * deltaRotation;

                m_TopLandAdditions.rotation = m_TopLandAdditions.rotation * deltaRotation;

                if( !m_HideWaypoints ){
                    if( lastEnabled && m_GameController.m_CurrentDistance > lastDistance ){
                        lastEnabled = false;
                        GameObject wp = Instantiate( m_LastWaypoint );
                        wp.transform.parent = m_ObstacleSpawner.m_ObstaclesParent;
                    }

                    if( averageEnabled && m_GameController.m_CurrentDistance > averageDistance ){
                        averageEnabled = false;
                        GameObject wp = Instantiate( m_AverageWaypoint );
                        wp.transform.parent = m_ObstacleSpawner.m_ObstaclesParent;
                    }

                    if( highEnabled && m_GameController.m_CurrentDistance > currentTop ){
                        highEnabled = false;
                        GameObject wp = Instantiate( m_HighWaypoint );
                        wp.transform.parent = m_ObstacleSpawner.m_ObstaclesParent;
                    }
                }

                float coef = Mathf.Clamp01( additionalSpeed / 100 );
                float maxFilter = 0.05f;
                if( coef > 0.4 ){
                    m_Filter.Intensity = coef * maxFilter;
                }else{
                    m_Filter.Intensity = 0;
                }

            }
            yield return null;
        }
    }

    public int GetDifficulty(){
        int total = 0;

        total += (int)( m_LandSpeed % m_SpeedIncreaseStep );
        total += m_SmallObstacles;
        total += m_MediumObstacles;
        total += m_BigObstacles;
        
        return total;
    }

    public void IncreaseDifficulty( int howMuch ){
        int pointsLeft = howMuch;

        if( m_ObstacleSpawnInterval > 0.5 ){
            m_ObstacleSpawnInterval -= Random.Range(0, 0.5f);
            m_ObstacleSpawnInterval = Mathf.Max(m_ObstacleSpawnInterval, 0.5f);
        }
        while( pointsLeft > 0 ){
            if( m_LandSpeed < 55 ){
                m_LandSpeed += m_SpeedIncreaseStep;
            }else if( m_SmallObstacles > 1 && m_LandSpeed < 70){
                m_LandSpeed += m_SpeedIncreaseStep;
            }else{
                m_SmallObstacles++;
            }

            pointsLeft--;
        }
        // while( pointsLeft > 0 ){
        //     int index = Random.Range(0, m_Components.Length);
        //     string whatToIncrease = m_Components[index];

        //     Debug.Log("Increasing " + whatToIncrease);

        //     if( whatToIncrease == "landSpeed" ){
        //         m_LandSpeed += m_SpeedIncreaseStep;
        //     }
        //     if( whatToIncrease == "smallObstacles" && m_SmallObstacles < 5 ){
        //         m_SmallObstacles++;
        //     }
        //     if( whatToIncrease == "mediumObstacles" && m_MediumObstacles < 2 ){
        //         m_MediumObstacles++;
        //     }
        //     if( whatToIncrease == "bigObstacles" && m_BigObstacles < 1 ){
        //         m_BigObstacles++;
        //     }
        //     pointsLeft--;
        // }
    }

    public void DecreasedDifficulty( int howMuch ){
        int pointsLeft = howMuch;

        while( pointsLeft > 0 ){
            m_LandSpeed += m_SpeedIncreaseStep;

            if( m_LandSpeed > 40 ){
                m_LandSpeed -= m_SpeedIncreaseStep;
            }else{
                m_SmallObstacles--;
            }

            pointsLeft--;
        }

        // while( pointsLeft > 0 ){
        //     int index = Random.Range(0, m_Components.Length);
        //     string whatToIncrease = m_Components[index];

        //     Debug.Log("Decreasing " + whatToIncrease);

        //     if( whatToIncrease == "landSpeed" && m_LandSpeed > 40 ){
        //         m_LandSpeed -= m_SpeedIncreaseStep;
        //     }
        //     if( whatToIncrease == "smallObstacles" && m_SmallObstacles > 1 ){
        //         m_SmallObstacles--;
        //     }
        //     if( whatToIncrease == "mediumObstacles" ){
        //         m_MediumObstacles--;
        //     }
        //     if( whatToIncrease == "bigObstacles" ){
        //         m_BigObstacles--;
        //     }
        //     pointsLeft--;
        // }
    }
    
    public virtual void OnObstacleTriggerEnter(){
        if( m_ObstaclesEnabled ){
            m_TriggersPassed++;

            int small = m_SmallObstacles;
            int medium = m_MediumObstacles;
            int big = m_BigObstacles;

            if( m_TriggersPassed % 3 != 0 ){
                medium = 0;
            }

            if( m_TriggersPassed % 5 != 0 ){
                big = 0;
            }
        

            if( m_TriggersPassed % 7 == 0 && Random.value > 0.4f ){
                m_ObstacleSpawner.PlacePowerUps( 1 );
            }

            // m_ObstacleSpawner.PlaceRandomBlockOnRandomPosition( small, medium, big );

            if( m_TriggersPassed % 3 == 0 ){
                m_ObstacleSpawner.PlaceCoins(1);
            }
            if( GlobalGameManager.instance.m_DoubleCoinsPurchased ){
                if( m_TriggersPassed % 2 == 0 ){
                    m_ObstacleSpawner.PlaceCoins(1);
                }
            }

            if( m_GameController.m_Player1.HasEmptySlotForAnimal() || (m_GameController.m_Player2 != null && m_GameController.m_Player2.HasEmptySlotForAnimal()) ){
                if( m_TriggersPassed % 9 == 0 && Random.value > 0.4f ){
                    m_ObstacleSpawner.PlaceAnimals( 1 );
                }
            }

            if( m_GameController.m_Player1.HasAnimal() || (m_GameController.m_Player2 != null && m_GameController.m_Player2.HasAnimal()) ){
                if( m_TriggersPassed % 12 == 0 && Random.value > 0.4f ){
                    m_ObstacleSpawner.PlaceRockets( 1 );
                }
            }


        }
    }

    public virtual void OnCoinCollected(){}
    
    public virtual void OnSpecialAbilityActivated(){}

    public virtual void OnAnimalCollected(){}

    public virtual void OnRocketCollected(){}

    public void PowerUpCollected( PowerUp powerUp, MovementController mc ){
        powerUp.Apply(mc);
    }
    
    public void TmpSpeedBonus(){
        m_AdditionalLandSpeed1 = 40;
        StartCoroutine(Cooldown());
    }

    public void DisableObstaclesForSeconds( float seconds ){
        m_ObstaclesEnabled = false;

        StartCoroutine(DisableObstacles(seconds));
    }

    IEnumerator DisableObstacles( float seconds ){
        yield return new WaitForSeconds( seconds );
        m_ObstaclesEnabled = true;
    }

    IEnumerator Cooldown(){
        yield return new WaitForSeconds( 1f );
        m_AdditionalLandSpeed1 = 0;
    }
}
