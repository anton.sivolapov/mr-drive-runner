﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

using TimeSpan = System.TimeSpan;

public class GameController : MonoBehaviour {
    public bool m_Tutorial;

    [Header("Players Configuration")]
    public bool m_TwoPlayersGame;
    public GameObject m_PlayerPrefab;
    public Transform m_Player1SpawnPoint;
    public Transform m_Player2SpawnPoint;

    public MonoBehaviour m_Postprocessing1;
    public MonoBehaviour m_Postprocessing2;

    [Header("GUI")]
    public GUIController m_GUI;
    public MessagePopup m_MessagePopup;
    
    [Header("Sounds")]
    public AudioClip m_CoinPickup;
    public AudioClip m_Hit;
    public AudioClip m_Wreck;

    [HideInInspector]
    public int m_CoinsDelta;
    [HideInInspector]
    public int m_ScoreDelta;
    [HideInInspector]
    public int m_CurrentTime;

    [HideInInspector]
    public float m_CurrentDistance;
    [HideInInspector]
    public float m_LastDistance;

    // [HideInInspector]
    public MovementController m_Player1;
    // [HideInInspector]
    public MovementController m_Player2;

    [HideInInspector]
    public bool m_VideoWatched;
    [HideInInspector]
    public int m_CurrentCoins;
    [HideInInspector]
    public int m_CurrentScore;
    [HideInInspector]
    public float m_DeltaDistance;

    private AudioSource m_AudioSource;

    void Awake (){
        if( GlobalGameManager.instance.m_IsMultiplayerGame ){
            m_TwoPlayersGame = true;
        }
        m_LastDistance = GlobalGameManager.instance.m_LastDistance;


        Debug.Log("SystemInfo.processorFrequency:" + SystemInfo.processorFrequency.ToString());
        Debug.Log("SystemInfo.graphicsMemorySize:" + SystemInfo.graphicsMemorySize.ToString());
        Debug.Log("SystemInfo.systemMemorySize:" + SystemInfo.systemMemorySize.ToString());
#if UNITY_ANDROID
        bool processorOk =  SystemInfo.processorFrequency < 100 || SystemInfo.processorFrequency > 1500;
        bool graphicsMemoryOk =  SystemInfo.graphicsMemorySize < 100 || SystemInfo.graphicsMemorySize > 256;
        bool systemMemoryOk =  SystemInfo.systemMemorySize < 100 || SystemInfo.systemMemorySize > 900;

        if( !processorOk || !graphicsMemoryOk || !systemMemoryOk ){
            Debug.Log("==== low perfomance, decrease quality");
            // Application.targetFrameRate = 30;
            m_Postprocessing1.enabled = false;
            m_Postprocessing2.enabled = false;
        }
#endif
    }

    void Start () {
        GlobalGameManager.instance.m_NumberOfGameSessions++;

        GlobalGameManager.instance.m_LastDistance = 0;

        ResumeGame();

        m_AudioSource = GetComponent<AudioSource>();
        m_AudioSource.volume = 0.8f;

        InitPlayers();
        InitStats();

        if( m_Tutorial ){
            GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("Tutorial");
        }else{
            if( GlobalGameManager.instance.m_IsMultiplayerGame ){
                GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("MultiplayerGameScreen");
            }else{
                GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("SingleplayerGameScreen");
            }
        }
    }

    // Use this for initialization
    // void Start () {
    // }
    
    void InitStats(){
        //init stats 
        m_CurrentCoins = GlobalGameManager.instance.m_Coins;
        m_GUI.AddCoins(string.Format("{0:n0}", m_CurrentCoins));

        m_CurrentScore = GlobalGameManager.instance.m_Score;
        m_GUI.AddScore(string.Format("{0:n0}", m_CurrentScore));

        m_CurrentTime = 0;
        StartCoroutine("UpdateScore");
        StartCoroutine("UpdateTime");
        StartCoroutine("UpdateTotalDistance");
        // StartCoroutine("UpdateCoins");
    }
    
    void InitPlayers(){
        GameData gameData = GlobalGameManager.instance.m_GameData;
        int selectedIndex  = GlobalGameManager.instance.m_SelectedVehicleIndex;

        //first player init
        GameObject player1 = Instantiate(m_PlayerPrefab);
        player1.transform.SetParent(m_Player1SpawnPoint.parent);
        player1.transform.position = m_Player1SpawnPoint.position;
        m_Player1 = player1.GetComponent<MovementController>();
        m_Player1.m_PlayerNum = 1;

        GameObject playerRenderer1 = Instantiate(gameData.m_Vehicles[selectedIndex].m_Renderer);
        playerRenderer1.transform.parent = m_Player1.m_PlayerContainer.transform;
        Vector3 newPos = playerRenderer1.transform.localPosition;
        newPos.x = 0;
        newPos.z = 0;
        playerRenderer1.transform.localPosition = newPos;
        
        m_Player1.m_PlayerRenderer = playerRenderer1;
        m_Player1.InitPlayer();

        if (m_TwoPlayersGame){
            //second player init
            GameObject player2 = Instantiate(m_PlayerPrefab);
            player2.transform.SetParent(m_Player2SpawnPoint.parent);
            player2.transform.position = m_Player2SpawnPoint.position;
            m_Player2 = player2.GetComponent<MovementController>();
            m_Player2.m_PlayerNum = 2;

            GameObject playerRenderer2 = Instantiate(gameData.m_Vehicles[selectedIndex].m_Renderer);
            playerRenderer2.transform.parent = m_Player2.m_PlayerContainer.transform;
            playerRenderer2.transform.localPosition = Vector3.zero;

            m_Player2.m_PlayerRenderer = playerRenderer2;
            m_Player2.InitPlayer();
        }
    }
    
    void ResurrectPlayers(){
        m_Player1.MakeInvincible();
        m_Player1.Reset();
        if (m_TwoPlayersGame){
            m_Player2.MakeInvincible();
            m_Player2.Reset();
        }
    }

    IEnumerator UpdateTotalDistance () {
        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }

            if( !m_Tutorial ){
                GlobalGameManager.instance.m_TotalDistance += m_DeltaDistance; 
                m_DeltaDistance = 0;

                GlobalGameManager.instance.m_LastDistance = m_CurrentDistance;
            }

            yield return new WaitForSeconds(1f);
        }
    }

    //deprecated
    IEnumerator UpdateCoins () {
        int increment = 1;
        float updateTime = 1f;
        float coef = 1;

        if( GlobalGameManager.instance.m_DoubleCoinsPurchased ){
            coef *= 2f;
        }
        if( m_Player2 != null ){
            coef *= 2f;
        }

        increment = (int)Mathf.Round( increment * coef );
        increment += GlobalGameManager.instance.m_SelectedVehicle.m_AdditionalCoins;

        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }

            m_CoinsDelta += increment;
            GlobalGameManager.instance.m_Coins += increment;
            m_CurrentCoins = GlobalGameManager.instance.m_Coins;
            m_GUI.AddCoins(string.Format("{0:n0}", m_CurrentCoins));
            yield return new WaitForSeconds(updateTime);
        }
    }

    IEnumerator UpdateScore () {
        int increment = 1;
        float updateTime = 1f;
        float coef = 1f;

        if( m_Player2 != null ){
            coef *= 2f;
        }

        increment = (int)Mathf.Round( increment * coef );
        increment += GlobalGameManager.instance.m_SelectedVehicle.m_AdditionalScore;

        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            if( !m_Tutorial ){
                m_ScoreDelta += increment;
                m_CurrentScore += increment;
            }
            m_GUI.AddScore(string.Format("{0:n0}", m_CurrentScore));

            yield return new WaitForSeconds(updateTime);
        }
    }

    IEnumerator UpdateTime () {
        while( true ){
            while( GlobalGameManager.paused ){
                yield return null;
            }
            m_CurrentTime++;
            TimeSpan timeSpan = TimeSpan.FromSeconds(m_CurrentTime);
            // Debug.Log(timeSpan);
            string timeText = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
            m_GUI.AddTime(timeText);
            yield return new WaitForSeconds(1f);
        }
    }

    public void PauseGame () {
        GetComponent<AudioSource>().volume = 0.2f;
        GlobalGameManager.paused = true;
    }

    public void UnpauseGame () {
        GlobalGameManager.paused = false;
    }

    public void ResumeGame () {
        UnpauseGame();
        GetComponent<AudioSource>().volume = 0.8f;
    }

    void UsePotion () {
        ResumeGame();
        ResurrectPlayers();

        if( GlobalGameManager.instance.m_IsMultiplayerGame ){
            GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("MultiplayerGameScreen");
        }else{
            GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("SingleplayerGameScreen");
        }
    }

    void GoHomeScreen () {
        UnpauseGame();
        LoadingScreenManager.LoadScene(0);
    }

    void GoHomeScreenFromGameOver () {
        if( GlobalGameManager.instance.m_IsMultiplayerGame ){
            Analytics.CustomEvent("multiplayerGameOver", new Dictionary<string, object>
                    {
                        { "coins", m_CoinsDelta },
                        { "score", m_ScoreDelta },
                        { "time", m_CurrentTime },
                        { "vehicle", m_Player1.m_PlayerRenderer.name }
                    });
            GlobalGameManager.instance.m_GoogleAnalytics.LogTiming("MultiplayerGameplay", m_CurrentTime, "MultiplayerGame", "Finish");
        }else{
            //if first session, reach achivement
            GlobalGameManager.instance.UpdateAchivement("first_spin", 100d);
            Analytics.CustomEvent("gameOver", new Dictionary<string, object>
                    {
                        { "coins", m_CoinsDelta },
                        { "score", m_ScoreDelta },
                        { "time", m_CurrentTime },
                        { "vehicle", m_Player1.m_PlayerRenderer.name }
                    });
            GlobalGameManager.instance.m_GoogleAnalytics.LogTiming("SingleplayerGameplay", m_CurrentTime, "SingleplayerGame", "Finish");
        }

        //if total coins more then 5000
        GlobalGameManager.instance.UpdateAchivement("richman", GlobalGameManager.instance.m_TotalCoins/5000);
        GlobalGameManager.instance.UpdateAchivement("top_player", GlobalGameManager.instance.m_Score/10000);
        GoHomeScreen();
    }

    void UpdateGUI () {
        BroadcastMessage("RefreshGUI", SendMessageOptions.DontRequireReceiver);
    }

    void OnPlayerHit ( MovementController mc ) {
        // m_GUI.DecreaseHealth();
        // Debug.Log("== OnPlayerHit");
        m_AudioSource.PlayOneShot(m_Hit, 0.7f);
    }

    void OnPlayerDied () {
        m_AudioSource.PlayOneShot(m_Wreck, 0.5f);

        PauseGame();
        Invoke("FinishGame", 1);
    }
    
    void FinishGame (){
        // GlobalGameManager.instance.m_AdManager.ShowInterstitialAd();

        if( !GlobalGameManager.instance.m_AdsRemoverPurchased && GlobalGameManager.instance.m_NumberOfGameSessions % 3 == 0 ){
            GlobalGameManager.instance.m_AdManager.ShowInterstitialAd();
        }


        if( !m_Tutorial ){
            GlobalGameManager.instance.m_Score = m_CurrentScore;
            if( GlobalGameManager.instance.m_TopDistance < m_CurrentDistance ){
                GlobalGameManager.instance.m_TopDistance = m_CurrentDistance;
            }
        }

        if( GlobalGameManager.instance.m_IsMultiplayerGame ){
            GlobalGameManager.instance.UpdateMultiplayerLeaderboard( (int)m_CurrentDistance);
        }else{
            GlobalGameManager.instance.UpdateDistanceLeaderboard( (int)m_CurrentDistance);
        }

        GlobalGameManager.instance.UpdateScoreLeaderboard(GlobalGameManager.instance.m_Score);

        m_GUI.ShowGameOverPopup();
    }

    void CoinCollected ( int value ) {
        m_AudioSource.PlayOneShot(m_CoinPickup, 1f);
        m_CoinsDelta += value;

        GlobalGameManager.instance.m_Coins += value;
        GlobalGameManager.instance.m_TotalCoins += value;

        m_CurrentCoins = GlobalGameManager.instance.m_Coins;
    }
    
    public void OnWorldRotated( float worldSpeed ){
        float distanceFromLastFrame = worldSpeed / 7f * Time.deltaTime;

        if( !m_Tutorial ){
            m_CurrentDistance += distanceFromLastFrame;
            m_DeltaDistance += distanceFromLastFrame;
        }
    }
}
