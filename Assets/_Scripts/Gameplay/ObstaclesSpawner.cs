﻿using Action = System.Action;
using String = System.String;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BridgeDriver;

public class LineInfo{
    public int[] m_Draft;
    public string m_DraftString;
    public int[] m_Windows;
    public List<ObstacleWindow> m_WindowsInfo;
    
    public LineInfo(){
        m_WindowsInfo = new List<ObstacleWindow>();
    }
}

public struct ObstacleWindow  
{  
    public int startPos;  
    public int size;  
    public ObstacleWindow(int i, int s)
    {
        startPos = i;
        size = s;
    }
} 

public class ObstaclesSpawner : MonoBehaviour {
    public GameObject[] m_ObstaclesList;
    public GameObject[] m_SpawnAreas;
    public GameObject m_SpawnArea;
    public GameObject m_RandomSpawnArea;
    public GameObject m_CoinPrefab;
    public GameObject m_TopLand;
    public Transform m_ObstablesParent;
    public float m_ObstacleAppearenceTime = 1f;
    public int m_LevelNum = 0;
    public int m_NumberOfObstacles = 1;

    //we need this trigger to notify about passing line of obstacles
    public GameObject m_PassTrigger;

    public GameObject m_Center;
    
    private LevelTheme m_Theme;
    private Obstacle[] m_ListOfObstacles;
    private int unitSize;
    private int totalSizeUnits;
    private int m_ObstaclesPassed;
    private Obstacle m_LastObstacle;

    // Use this for initialization
    void Start () {
        Vehicle vehicleTmp = GlobalGameManager.instance.m_SelectedVehicle;
        m_Theme = vehicleTmp.m_PossibleLevelThemes[Random.Range(0, vehicleTmp.m_PossibleLevelThemes.Length)];

        m_TopLand.GetComponent<MeshRenderer>().material = m_Theme.m_LandMaterial;

        m_ListOfObstacles = m_Theme.m_SmallObstacles.m_Items;
        unitSize = (int)GlobalGameManager.instance.m_GameData.m_OneUnitObstacle.localScale.x;

        Instantiate(m_Theme.m_CenterPrefab, m_Center.transform);

        //total size of game field
        //total size in units
        totalSizeUnits = (int)(m_SpawnArea.transform.localScale.x / unitSize);

        // GenerateObstacleLine( m_LevelNum );
        // StartCoroutine("SpawnRandomObstacles");
        
        // PlaceBlock(0, 2, m_ListOfObstacles[25].m_Prefab);
        // PlaceBlock(1, 1, m_ObstaclesList[0]);
        // PlaceBlock(0, 4, m_ObstaclesList[1]);
    }

    public void OnObstacleTriggerEnter(){
        m_ObstaclesPassed++;
        // Debug.Log("OnObstacleTriggerEnter");
        // int num = Random.Range(1, m_NumberOfObstacles);
        for( int i = 0; i < m_NumberOfObstacles; i++ ){
            PlaceRandomBlock();
        }
        m_LastObstacle = null;
        PlaceRandomCoin();
    }

    //return array representation of line of obstacles, according to level num
    LineInfo GetLineDraft( int levelNum ){
        LineInfo info = new LineInfo();
        if( levelNum >= GlobalGameManager.instance.m_GameData.m_ObstacleLevels.Length )
            levelNum = GlobalGameManager.instance.m_GameData.m_ObstacleLevels.Length - 1;

        ObstacleLevel level = GlobalGameManager.instance.m_GameData.m_ObstacleLevels[levelNum];

        //percent of clean space
        float windowSizeUnits = level.m_WindowSize;
        int numberOfWindows = level.m_NumberOfWindows;
        int minimumWinSize = level.m_MinimumWinSize;
        
        //first level draft with whole empty line
        //0 - solid block, 1 - empty block
        int[] lineDraft = new int[totalSizeUnits];
        for( int i = 0; i < lineDraft.Length; i++ ){
            lineDraft[i] = 1;
        }

        int[] randomWins = new int[numberOfWindows];
        float[] tmp_randomWins = new float[numberOfWindows];
        float sum = 0;
        for( int i = 0; i < numberOfWindows; i++ ){
            tmp_randomWins[i] = Random.Range(minimumWinSize, windowSizeUnits/numberOfWindows + minimumWinSize);
            sum += tmp_randomWins[i];
        }

        for( int i = 0; i < numberOfWindows; i++ ){
            randomWins[i] = (int)Mathf.Round( tmp_randomWins[i]/sum * windowSizeUnits );
        }
        
        int availableUnits = totalSizeUnits;
        int currentUnit = 0;
        int winNumbers = numberOfWindows;
        for( int i = 0; i < numberOfWindows; i++ ){
            winNumbers -= i;
            //get win size
            int winSize = 0;
            for( int y = i; y < numberOfWindows; y++ ){
                winSize += randomWins[y];
            }
            int winStartPosition = Random.Range( currentUnit, totalSizeUnits - ( winSize + winNumbers - 1 ) );
            int winEndPosition = winStartPosition + randomWins[i];
            currentUnit = winEndPosition + 1;

            for( int y = winStartPosition; y < winEndPosition; y++ ){
                lineDraft[y] = 0;
            }

            // Debug.Log("win. from " + winStartPosition + ". to " + winEndPosition );

            info.m_WindowsInfo.Add( new ObstacleWindow(winStartPosition, randomWins[i]) );

            availableUnits -= winSize;
        }
        
        // info.m_Windows = randomWins;
        info.m_Draft = lineDraft;
        foreach( int i in lineDraft ){
            info.m_DraftString += i.ToString();
        }
        return info;
    }

    

    //generate full line of obstacles according to level
    void GenerateObstacleLine ( int level ){
        LineInfo draft = GetLineDraft(level);
        string draftStr = draft.m_DraftString;
        Debug.Log(draftStr);

        List<Obstacle> possibleObstacles = new List<Obstacle>();
        foreach( Obstacle o in m_ListOfObstacles ){
            string obCode = o.m_StringCode;
            if( obCode.Length != o.m_Width ){
                if( obCode.Length == 1 ){
                    obCode = new String(obCode[0], o.m_Width);
                }else{
                    Debug.LogWarning("Bad string code for obstacle " + o.m_Prefab.name);
                }
            }

            if( draftStr.IndexOf( obCode ) != -1 ){
                o.m_StringCode = obCode;
                possibleObstacles.Add(o);
            }
        }
        Utils.Shuffle(possibleObstacles);

        //sort
        // possibleObstacles.Sort( (a, b) => a.m_StringCode.Length > b.m_StringCode.Length ? -1 : 1 );


        List<Obstacle> lineOfObstacles = new List<Obstacle>();
        List<int> positions = new List<int>();
        int count = 0;
        while( ( draftStr.IndexOf('0') != -1 || draftStr.IndexOf('1') != -1 ) && count < 10){
            foreach( Obstacle o in possibleObstacles ){
                int startPos = draftStr.IndexOf( o.m_StringCode );
                if( startPos != -1 ){
                    draftStr = draftStr.Remove(startPos, o.m_StringCode.Length).Insert(startPos, o.m_StringCode.Replace(o.m_StringCode, new String('x', o.m_StringCode.Length)));
                    lineOfObstacles.Add(o);
                    positions.Add(startPos);
                }
            }
            count++;
        }

        for( int i = 0; i < positions.Count; i++ ){
            PlaceBlock(positions[i], lineOfObstacles[i].m_StringCode.Length, lineOfObstacles[i].m_Prefab, m_SpawnArea);
        }

        //place pass trigger
        Vector3 spawnBox = m_SpawnArea.transform.localScale;
        GameObject passTrigger = Instantiate(m_PassTrigger, m_ObstablesParent);
        passTrigger.GetComponentInChildren<PassTriggerController>().m_ObstacleSpawner = this;
        Vector3 newPosition = Vector3.zero;
        passTrigger.transform.rotation = m_SpawnArea.transform.rotation;

    }

    void PlaceBlock( int position, int size, GameObject prefab, GameObject spawnArea ){
        Vector3 spawnBox = spawnArea.transform.localScale;
        GameObject obstacle = Instantiate(prefab);
        //reset
        obstacle.transform.parent = m_ObstablesParent;
        // obstacle.transform.localScale = Vector3.one;

        //place renderer into center of parent
        Renderer renderer = obstacle.GetComponentInChildren<Renderer>();
        Mesh mesh = obstacle.GetComponentInChildren<MeshFilter>().mesh;
        Collider collider = obstacle.GetComponentInChildren<Collider>();
        if( collider == null ){
            collider = renderer.gameObject.AddComponent<MeshCollider>( ) as Collider;
            // collider.convex = true;
        }
        collider.gameObject.layer = 9; //"Obstacle"
        // renderer.gameObject.AddComponent<ObstacleController>();
        collider.gameObject.tag = "Obstacle";

        float scaleFactor = unitSize * size / renderer.bounds.size.x;

        Vector3 newPos = renderer.gameObject.transform.position;
        obstacle.transform.localScale = new Vector3(
                                                    obstacle.transform.localScale.x * scaleFactor,
                                                    obstacle.transform.localScale.y * scaleFactor,
                                                    obstacle.transform.localScale.z * scaleFactor
                                                    );

        //set new center
        Vector3 center = mesh.bounds.center;

        newPos.x = -center.x * renderer.gameObject.transform.localScale.x;
        if( renderer.gameObject.transform.position.y == 0 ){
            newPos.y = mesh.bounds.min.y * renderer.gameObject.transform.localScale.y;
        }else{
            // Debug.LogWarning(obstacle.name + " has adjust center on Y, left it untouched");
            // newPos.y = renderer.gameObject.transform.position.y * renderer.gameObject.transform.localScale.y;
            // newPos.y = renderer.gameObject.transform.position.y * renderer.gameObject.transform.localScale.y;
        }
        newPos.z = -center.z * renderer.gameObject.transform.localScale.z;
        renderer.gameObject.transform.localPosition = newPos;

        Vector3 newPosition = Vector3.one;
        newPosition.x = position * unitSize + unitSize * size /2;
        newPosition.y = spawnBox.y - renderer.bounds.size.y/2 - Random.Range(0f, renderer.bounds.size.y);
        newPosition.z = spawnBox.z/2;

        newPosition = spawnArea.transform.TransformPointUnscaled(newPosition - spawnBox/2);
        obstacle.transform.position = newPosition;

        Quaternion rot = obstacle.transform.rotation * spawnArea.transform.rotation;
        obstacle.transform.rotation = rot;
        StartCoroutine("AnimateAppearance", obstacle);
    }

    void PlaceObject( int position, GameObject prefab, GameObject spawnArea){
        Vector3 spawnBox = spawnArea.transform.localScale;
        GameObject obstacle = Instantiate(prefab);
        //reset
        obstacle.transform.parent = m_ObstablesParent;
        // obstacle.transform.localScale = Vector3.one;

        //place renderer into center of parent
        Renderer renderer = obstacle.GetComponentInChildren<Renderer>();
        Mesh mesh = obstacle.GetComponentInChildren<MeshFilter>().mesh;
        Collider collider = obstacle.GetComponentInChildren<Collider>();
        if( collider == null ){
            collider = renderer.gameObject.AddComponent<MeshCollider>( ) as Collider;
            // collider.convex = true;
        }
        collider.gameObject.layer = 9; //"Obstacle"
        // renderer.gameObject.AddComponent<ObstacleController>();
        if( collider.gameObject.tag == "Untagged")
            collider.gameObject.tag = "Obstacle";

        Vector3 newPosition = Vector3.one;
        newPosition.x = position * unitSize;
        newPosition.y = spawnBox.y - renderer.bounds.size.y/2 - Random.Range(0f, renderer.bounds.size.y);
        newPosition.z = spawnBox.z/2;

        newPosition = spawnArea.transform.TransformPointUnscaled(newPosition - spawnBox/2);
        obstacle.transform.position = newPosition;

        Quaternion rot = obstacle.transform.rotation * m_SpawnArea.transform.rotation;
        obstacle.transform.rotation = rot;
        StartCoroutine("AnimateAppearance", obstacle);
    }
    void PlaceRandomBlock(){
        //only spawn new obstacle, when latest not too big
        if( m_LastObstacle == null || m_LastObstacle.m_StringCode.Length <= 3 ){
            List<Obstacle> possibleObstacles = m_Theme.m_SmallObstacles.m_Items.ToList<Obstacle>();
            Utils.Shuffle(possibleObstacles);
            GameObject spawn = m_SpawnAreas[m_ObstaclesPassed % m_SpawnAreas.Length];

            m_LastObstacle = possibleObstacles[0];
            PlaceBlock(Random.Range(0, totalSizeUnits), possibleObstacles[0].m_StringCode.Length, possibleObstacles[0].m_Prefab, spawn);
        }
    }

    void PlaceRandomCoin(){
        GameObject spawn = m_SpawnAreas[( m_ObstaclesPassed + 1 ) % m_SpawnAreas.Length];
        PlaceObject(Random.Range(0, totalSizeUnits), m_CoinPrefab, spawn);
    }

    IEnumerator AnimateAppearance( GameObject obstacle){
        // Debug.Log("animate " + obstacle.name);
        Mesh mesh = obstacle.GetComponentInChildren<MeshFilter>().mesh;

        float halfY = 0;
        float delta = halfY - obstacle.transform.localPosition.y;

        while( obstacle.transform.localPosition.y < halfY ){
            Vector3 updatedPos = obstacle.transform.localPosition;
            updatedPos.y +=  delta * Time.deltaTime / m_ObstacleAppearenceTime * Random.value;
            if( updatedPos.y > halfY ){
                updatedPos.y = halfY;
            }

            obstacle.transform.localPosition = updatedPos;

            yield return null;
        }
    }
    
    public void LinePassed(){
        GenerateObstacleLine( m_LevelNum );
        SendMessageUpwards("OnPassedLineOfObstacles");
    }

    IEnumerator SpawnRandomObstacles () {
        while( true ){
            yield return new WaitForSeconds(Random.Range( 1f, 3f ));
            PlaceRandomBlock();
        }
    }
}
