﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {
    public int m_Value;

    void OnTriggerEnter(Collider other) {
        if( other.gameObject.tag == "Player" ){
            other.gameObject.GetComponentInParent<MovementController>().OnCoinsCollected(m_Value);
            Destroy(gameObject);
        }
    }
}
