﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour {
    public PowerUp m_PowerUp;

    private DifficultyManager m_DifficultyManager;

    void Start(){
        m_DifficultyManager = FindObjectOfType<DifficultyManager>();
    }

    void OnTriggerEnter(Collider other) {
        if( other.gameObject.tag == "Player" ){
            m_DifficultyManager.PowerUpCollected( m_PowerUp, other.gameObject.GetComponentInParent<MovementController>() );
            // Destroy(gameObject);
        }
    }
}
