﻿using System.Linq;
using Array = System.Array;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObstacleSpawnerV2 : MonoBehaviour {
    public GameObject m_SmallObstaclesSpawnList;
    public GameObject m_MediumObstaclesSpawnList;
    public GameObject m_BigObstaclesSpawnList;
    public GameObject m_CoinsSpawnList;
    public GameObject m_PowerupsSpawnList;
    public GameObject m_AnimalsSpawnList;
    public GameObject m_RocketsSpawnList;

    public Transform m_ObstaclesParent;
    public GameObject m_Center;
    public GameObject m_TopLand;
    public GameObject m_BottomLand;

    //private
    private LevelTheme m_Theme;
    private Vehicle m_CurrentVehicle;
    private GameData m_GameData;

    // Use this for initialization
    void Start () {
        m_CurrentVehicle = GlobalGameManager.instance.m_SelectedVehicle;
        int index = Random.Range(0, m_CurrentVehicle.m_PossibleLevelThemes.Length);
        m_Theme = m_CurrentVehicle.m_PossibleLevelThemes[index];
        
        m_GameData = GlobalGameManager.instance.m_GameData;

        Instantiate(m_Theme.m_CenterPrefab, m_Center.transform);
        if( m_Theme.m_Sky != null ){
            GameObject sky = GameObject.Find("Sky");
            if( sky != null ){
                sky.GetComponent<Image>().sprite = m_Theme.m_Sky;
            }
        }

        m_TopLand.GetComponent<MeshRenderer>().material     = m_Theme.m_LandMaterial;
        m_BottomLand.GetComponent<MeshRenderer>().material  = m_Theme.m_LandBottomMaterial;
    }

    /**
     * Return only available points, to avoid spawn two objects at the same point
     **/
    ObstacleSpawnPoint[] getSpawnPoints( GameObject obstaclesList ){
        ObstacleSpawnPoint[] allPoints = obstaclesList.GetComponentsInChildren<ObstacleSpawnPoint>();

        ObstacleSpawnPoint[] available = Array.FindAll(allPoints, p => p.m_IsAvailable);
        return available;
    }

    void makeAllSpawnPointsAvailable( GameObject obstaclesList ){
        ObstacleSpawnPoint[] allPoints = obstaclesList.GetComponentsInChildren<ObstacleSpawnPoint>();
        Array.ForEach(allPoints, p => p.m_IsAvailable = true);
    }

    void PlaceBlock( Vector3 position, GameObject obstacle ){
        //add collider, assign tag and physics layer
        // Renderer renderer = obstacle.GetComponentInChildren<Renderer>();
        Collider[] colliders = obstacle.GetComponentsInChildren<Collider>();
        if( colliders != null ){
            foreach( Collider collider in colliders){
                collider.gameObject.layer = 9; //"Obstacle"

                if( collider.gameObject.tag == "Untagged" ){
                    collider.gameObject.tag = "Obstacle";
                }
            }
        }

        obstacle.transform.parent = m_ObstaclesParent;

        //rotate obstacle
        float angle = -1f * Mathf.Atan2( position.z, position.x ) * Mathf.Rad2Deg;
        Vector3 rotation = obstacle.transform.rotation.eulerAngles;
        rotation.y += angle;
        obstacle.transform.rotation = Quaternion.Euler(rotation);

        Centerer centerer = obstacle.GetComponent<Centerer>();
        if( centerer != null ){
            position.y = centerer.m_Center.y;
        }
        obstacle.transform.position = position;

        Vector3 targetScale = obstacle.transform.localScale;
        obstacle.transform.localScale = Vector3.zero;

        float appearTime = Random.Range(0.4f, 1f);
        string[] appearTypes = new string[3] {
            "easeOutBounce",
            "easeInOutQuad",
            "easeInOutQuart"
        };
        string appearType = appearTypes[Random.Range(0, appearTypes.Length)];

        iTween.ScaleTo(obstacle, iTween.Hash("scale", targetScale, "easeType", appearType, "time", appearTime ));
    }

    void PlaceGenericBlock( Vector3 position, GameObject obstacle ){
        //add collider, assign tag and physics layer
        obstacle.transform.parent = m_ObstaclesParent;

        //rotate obstacle
        float angle = -1f * Mathf.Atan2( position.z, position.x ) * Mathf.Rad2Deg;
        Vector3 rotation = obstacle.transform.rotation.eulerAngles;
        rotation.y += angle;
        obstacle.transform.rotation = Quaternion.Euler(rotation);

        // position.y = obstacle.GetComponent<Centerer>().m_Center.y;
        obstacle.transform.position = position;
        Vector3 targetScale = obstacle.transform.localScale;
        obstacle.transform.localScale = Vector3.zero;


        // iTween.ScaleTo(obstacle, targetScale, 2f);

        // iTween.ScaleTo(obstacle, targetScale, 2f);
        // iTween.ScaleTo(obstacle, iTween.Hash("scale", targetScale, "easeType", "easeInOutBack", "time", 1f ));
        iTween.ScaleTo(obstacle, iTween.Hash("scale", targetScale, "easeType", "easeOutBounce", "time", 1f ));

        // StartCoroutine("AnimateAppearance", obstacle);
    }
    
    IEnumerator AnimateAppearance( GameObject obstacle){
        Vector3 position = obstacle.transform.position;
        Vector3 center = obstacle.GetComponent<Centerer>().m_Center;
        position.y = -center.y;
        obstacle.transform.position = position;
        

        float startY = obstacle.transform.position.y;
        float endY = center.y;

        float startTime = Time.time;
        float speed = Random.Range(0.4f, 0.8f);

        float distCovered = (Time.time - startTime) / speed;
        while( distCovered < 1f ){
            distCovered = (Time.time - startTime) / speed;
            Vector3 newPos = obstacle.transform.position;

            newPos.y = Mathf.Lerp(startY, endY, distCovered);
            obstacle.transform.position = newPos;

            yield return null;
        }
    }

    void PlaceSmallObstacle(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_SmallObstaclesSpawnList); 
        //all spawn points available again
        makeAllSpawnPointsAvailable(m_SmallObstaclesSpawnList);

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        // int spawnPointIndex = 0;
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];
        spawnPoint.m_IsAvailable = false;

        List<Obstacle> possibleObstacles = m_Theme.m_SmallObstacles.m_Items.ToList<Obstacle>();
        int obstacleIndex = Random.Range(0, possibleObstacles.Count);
        // int obstacleIndex = 20;
        Obstacle candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate.m_Prefab);
        // ObstacleController oc = candidate.m_Prefab.GetComponent<ObstacleController>()
        //     .GetPooledInstance<ObstacleController>();
        // GameObject obstacle = oc.gameObject;

        if( spawnPointIndex < allPoints.Length / 2 )
            obstacle.GetComponent<Centerer>().AlignRight();

        if( spawnPointIndex > allPoints.Length / 2 )
            obstacle.GetComponent<Centerer>().AlignLeft();

        

        PlaceBlock(spawnPoint.transform.position, obstacle);
    }


    void PlaceSmallObstacle( Vector3 position ){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_SmallObstaclesSpawnList); 
        int spawnPointIndex = Random.Range(0, allPoints.Length);
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];

        List<Obstacle> possibleObstacles = m_Theme.m_SmallObstacles.m_Items.ToList<Obstacle>();
        int obstacleIndex = Random.Range(0, possibleObstacles.Count);

        Obstacle candidate = possibleObstacles[obstacleIndex];
        GameObject obstacle = Instantiate(candidate.m_Prefab);

        // position.x = spawnPoint.transform.position.x;
        PlaceBlock(position, obstacle);
    }

    void PlaceMediumObstacle(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_MediumObstaclesSpawnList); 
        //all spawn points available again
        makeAllSpawnPointsAvailable(m_MediumObstaclesSpawnList);

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        // int spawnPointIndex = 0;
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];
        spawnPoint.m_IsAvailable = false;
        List<Obstacle> possibleObstacles = m_Theme.m_MediumObstacles.m_Items.ToList<Obstacle>();
        int obstacleIndex = Random.Range(0, possibleObstacles.Count);
        // int obstacleIndex = 20;
        Obstacle candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate.m_Prefab);
        if( spawnPointIndex < allPoints.Length / 2 )
            obstacle.GetComponent<Centerer>().AlignRight();

        if( spawnPointIndex > allPoints.Length / 2 )
            obstacle.GetComponent<Centerer>().AlignLeft();

        PlaceBlock(spawnPoint.transform.position, obstacle);
    }

    void PlaceBigObstacle(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_BigObstaclesSpawnList); 
        //all spawn points available again
        // makeAllSpawnPointsAvailable(m_BigObstaclesSpawnList);

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        // int spawnPointIndex = 0;
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];
        // spawnPoint.m_IsAvailable = false;
        List<Obstacle> possibleObstacles = m_Theme.m_BigObstacles.m_Items.ToList<Obstacle>();
        int obstacleIndex = Random.Range(0, possibleObstacles.Count);
        // int obstacleIndex = 20;
        Obstacle candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate.m_Prefab);

        PlaceBlock(spawnPoint.transform.position, obstacle);
    }

    void PlaceCoin(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_CoinsSpawnList); 
        //all spawn points available again
        makeAllSpawnPointsAvailable(m_CoinsSpawnList);

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];
        spawnPoint.m_IsAvailable = false;

        Obstacle[] possibleObstacles = m_GameData.m_Coins.m_Items;
        int obstacleIndex = Random.Range(0, possibleObstacles.Length);
        Obstacle candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate.m_Prefab);
        // CoinController cc = candidate.m_Prefab.GetComponent<CoinController>();
        // CoinController coin = cc.GetPooledInstance<CoinController>();
        // GameObject obstacle = coin.gameObject;

        PlaceGenericBlock(spawnPoint.transform.position, obstacle);
    }

    void PlaceAnimal(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_AnimalsSpawnList); 

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];

        GameObject[] possibleObstacles = m_CurrentVehicle.m_Animals;
        int obstacleIndex = Random.Range(0, possibleObstacles.Length);
        GameObject candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate);
        obstacle.GetComponent<Animal>().m_AnimalPrefab = candidate;
        obstacle.GetComponent<Animal>().SayHelp();

        PlaceGenericBlock(spawnPoint.transform.position, obstacle);
    }

    void PlaceRocket(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_RocketsSpawnList); 

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];

        Obstacle[] possibleObstacles = m_GameData.m_Rockets.m_Items;
        int obstacleIndex = Random.Range(0, possibleObstacles.Length);
        Obstacle candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate.m_Prefab);

        // Rocket rocket = candidate.m_Prefab.GetComponent<Rocket>().GetPooledInstance<Rocket>();
        // GameObject obstacle = rocket.gameObject;

        PlaceGenericBlock(spawnPoint.transform.position, obstacle);
    }

    void PlacePowerUp(){
        ObstacleSpawnPoint[] allPoints = getSpawnPoints(m_PowerupsSpawnList); 
        //all spawn points available again
        makeAllSpawnPointsAvailable(m_PowerupsSpawnList);

        int spawnPointIndex = Random.Range(0, allPoints.Length);
        ObstacleSpawnPoint spawnPoint = allPoints[spawnPointIndex];
        spawnPoint.m_IsAvailable = false;
        

        Obstacle[] possibleObstacles = m_GameData.m_PowerUps.m_Items;
        int obstacleIndex = Random.Range(0, possibleObstacles.Length);
        Obstacle candidate = possibleObstacles[obstacleIndex];
        
        GameObject obstacle = Instantiate(candidate.m_Prefab);

        PlaceGenericBlock(spawnPoint.transform.position, obstacle);
    }

    public void PlaceRandomBlockOnRandomPosition( int numSmallObstacles, int numMediumObstacles, int numBigObstacles ){

        for( int i = 0; i < numSmallObstacles; i++ ){
            PlaceSmallObstacle();
        }

        for( int i = 0; i < numMediumObstacles; i++ ){
            PlaceMediumObstacle();
        }

        for( int i = 0; i < numBigObstacles; i++ ){
            PlaceBigObstacle();
        }
    }

    public void PlaceRandomBlockOnPlayerWay(){
        GameController m_GameController = GetComponentInParent<GameController>();
        Vector3 newPos = Vector3.zero;
        //30 degrees
        newPos.x = -m_GameController.m_Player1.m_ObjectPoint.transform.position.x * 0.9880316240928618f;
        newPos.z = 70 * 0.15425144988758405f;

        PlaceSmallObstacle( newPos );
    }

    public void PlaceCoins( int numCoins ){
        for( int i = 0; i < numCoins; i++ ){
            PlaceCoin();
        }
    }

    public void PlacePowerUps( int numPowerups ){
        for( int i = 0; i < numPowerups; i++ ){
            PlacePowerUp();
        }
    }

    public void PlaceAnimals( int numAnimals ){
        for( int i = 0; i < numAnimals; i++ ){
            PlaceAnimal();
        }
    }

    public void PlaceRockets( int numRockets ){
        for( int i = 0; i < numRockets; i++ ){
            PlaceRocket();
        }
    }
}
