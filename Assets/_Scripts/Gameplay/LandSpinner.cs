﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandSpinner : MonoBehaviour {
    public float m_SpinSpeed = 10;

    public float m_MinSpinSpeed = 10;
    public float m_MaxSpinSpeed = 40;

    //how fast speed will increase per second
    public float m_SpinIncrement = 5;

    //spinner for base
    public Transform m_SpinnerBase;
    public float m_SpinSpeedBase;

    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        m_SpinSpeed = Mathf.Clamp(m_SpinSpeed, m_MinSpinSpeed, m_MaxSpinSpeed);

        m_SpinnerBase.Rotate(new Vector3( 0f, -m_SpinSpeedBase, 0f ));

        Quaternion deltaRotation = Quaternion.Euler(new Vector3( 0f, -m_SpinSpeed, 0f ) * Time.deltaTime);
        transform.rotation = transform.rotation * deltaRotation;
    }
}
