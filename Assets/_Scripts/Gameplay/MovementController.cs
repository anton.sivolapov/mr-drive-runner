﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZCameraShake;


public class MovementController : MonoBehaviour {
    public int m_PlayerNum = 1;
    public GameObject m_ObjectPoint;
    public GameObject m_PlayerContainer;
    public Trailer m_Trailer;

    [HideInInspector]
    public GameObject m_PlayerRenderer;

    public GameObject m_SmokeParticles;
    public GameObject m_CoinsNumPrefab;
    public GameObject m_HitSmokePrefab;
    public GameObject m_Shield;
    public Text m_PlayerName;
    public float m_MovementSpeed;
    public float m_Distant = 0.06f;
    public float m_MinPosition = 0f;
    public float m_MaxPosition = 50f;
    public bool m_Invincible;
    public bool m_UseSmoke = true;
    
    public int m_Health;
    public SpecialAbility m_Special;
    public GameObject m_CoinsMagnet;

    public GameObject m_Animal1;
    public GameObject m_Animal2;
    public GameObject m_Animal3;

    [HideInInspector]
    public Renderer m_Renderer;
    [HideInInspector]
    public BurstSpecial m_BurstSpecial;

    private bool m_GoLeft;
    private bool m_GoRight;
    private GameController m_GameController;
    private VehicleBody m_VehicleBody;
    
    private bool m_EnableControls = true;
    private DifficultyManager m_DifficultyManager;

    void Awake(){
        m_DifficultyManager = FindObjectOfType<DifficultyManager>();
    }
    void Start() {
        Reset();

        m_GameController = FindObjectOfType<GameController>();
        m_BurstSpecial = GetComponent<BurstSpecial>();

        // m_Special = GetComponent<SpecialAbility>();
        if( m_GameController.m_TwoPlayersGame ){
            m_PlayerName.text = m_PlayerNum.ToString() + "P";
        }
    }

    public void Reset(){
        m_EnableControls = true;

        m_Health = GlobalGameManager.instance.m_SelectedVehicle.m_Health;
        m_MovementSpeed = GlobalGameManager.instance.m_SelectedVehicle.m_MovingSpeed;

        SendMessageUpwards("UpdateGUI");
    }

    public void InitPlayer(){
        m_VehicleBody = GetComponentInChildren<VehicleBody>();
        Renderer renderer = m_PlayerRenderer.GetComponentInChildren<Renderer>();
        m_Renderer = renderer;
        Vector3 newPos = renderer.bounds.center;
        newPos.z = renderer.bounds.min.z;
        newPos.y = renderer.bounds.min.y;
        
        string componentName =  GlobalGameManager.instance.m_SelectedVehicle.m_SpecialAbilityName;
        m_Special = gameObject.AddComponent( Type.GetType(componentName) ) as SpecialAbility;
        
        if( m_SmokeParticles != null && m_UseSmoke ){
            m_SmokeParticles.SetActive(true);
            m_SmokeParticles.transform.position = newPos;
        }

    }

    // Update is called once per frame
    void Update () {
        if( m_EnableControls &&  !GlobalGameManager.paused ){
            float axis = Input.GetAxis("Horizontal" + m_PlayerNum);
            // transform.Rotation
            Vector3 pos = m_ObjectPoint.transform.position;
            bool stop = false;

            if( axis > 0 || m_GoRight ){
                //steer right
                pos.x += m_MovementSpeed * Time.deltaTime;
            }else if( axis < 0 || m_GoLeft ){
                //steer left
                pos.x -= m_MovementSpeed * Time.deltaTime;
            }else{
                //steer straight
                stop = true;
            }
        
            pos.x = Mathf.Clamp(pos.x, m_MinPosition, m_MaxPosition);
            m_ObjectPoint.transform.position = pos;

        
            m_PlayerContainer.transform.LookAt( m_ObjectPoint.transform );
            Vector3 velocity = Vector3.zero;
            float dampTime = 0.1f;

            if (stop){
                Vector3 position = m_PlayerContainer.transform.position;
                Vector3 b = m_ObjectPoint.transform.position - new Vector3(position.x, m_ObjectPoint.transform.position.y, m_ObjectPoint.transform.position.z);
                Vector3 target = m_PlayerContainer.transform.position + b;
                m_PlayerContainer.transform.position = Vector3.SmoothDamp(m_PlayerContainer.transform.position, target, ref velocity, dampTime);
            }
            if (m_PlayerContainer.transform.position.x - m_ObjectPoint.transform.position.x > m_Distant){
                float x = m_ObjectPoint.transform.position.x + m_Distant;
                Vector3 position2 = m_PlayerContainer.transform.position;
                position2.x = x;
                m_PlayerContainer.transform.position = position2;
            }
            else if (m_PlayerContainer.transform.position.x - m_ObjectPoint.transform.position.x < -m_Distant){
                float x2 = m_ObjectPoint.transform.position.x - m_Distant;
                Vector3 position3 = m_PlayerContainer.transform.position;
                position3.x = x2;
                m_PlayerContainer.transform.position = position3;
            }
        
            if ( Input.GetKeyDown("space") ){
                m_Special.Activate();
            }
        }

    }

    public void GoLeft(){
        m_GoRight = false;
        m_GoLeft = true;
    }

    public void GoRight(){
        m_GoRight = true;
        m_GoLeft = false;
    }

    public void GoStraight(){
        m_GoLeft = false;
        m_GoRight = false;
    }
    
    public void OnCoinsCollected( int value ){
        SendMessageUpwards("CoinCollected", value);
        m_DifficultyManager.OnCoinCollected();
        SendCoinsNum(value, 0.5f);
    }
    
    void SendCoinsNum( int value, float time ){

        GameObject coinsNum = Instantiate(m_CoinsNumPrefab);
        Vector3 startPos = m_PlayerContainer.transform.position;
        startPos.y = 21;
        coinsNum.transform.position = startPos;
        coinsNum.GetComponentInChildren<Text>().text = "+" + value;

        Vector3 targetPos = coinsNum.transform.position;
        targetPos.y += 50f;

        iTween.MoveTo(coinsNum, iTween.Hash(
                                            "position", targetPos,
                                            "easeType", "easeInOutQuad",
                                            "time", time,
                                            "onComplete", "CompleteCoinsNumAppearence",
                                            "onCompleteTarget", gameObject,
                                            "onCompleteParams", coinsNum
                                            ));
    }

    void CompleteCoinsNumAppearence( GameObject coinsNum ){
        Destroy(coinsNum);
    }


    public void OnBodyCollisionEnter(Collider other) {
        // Debug.Log("OnBodyCollisionEnter: " + collision.transform.gameObject.name);
        if( other.transform.gameObject.tag == "Obstacle" ){
            // ContactPoint hit = collision.contacts[0];
            m_Health--;

            GameObject smoke = Instantiate(m_HitSmokePrefab, other.transform.gameObject.transform);
            smoke.transform.position = other.transform.position;

            CameraShaker cameraShaker = GameObject.FindObjectOfType<CameraShaker>();
            cameraShaker.ShakeOnce(5f, 10f, 0.5f, 0.5f);

            if( !m_Invincible ){
                if( HasAnimal() ){
                    DropAnimals();
                    MakeInvincible();
                }else{
                    if( m_Health <= 0 ){
                        gameObject.BroadcastAll("OnPlayerHit", this);
                        SendMessageUpwards("OnPlayerDied");
                        m_EnableControls = false;
                    }else{
                        SendMessageUpwards("UpdateGUI");
                        gameObject.BroadcastAll("OnPlayerHit", this);
                        MakeInvincible();
                    }
                }
            }
        }
    }
    public void OnBodyCollisionEnterOld(Collision collision) {
        if( collision.transform.gameObject.tag == "Obstacle" ){
            ContactPoint hit = collision.contacts[0];
            float angle = Vector3.Angle(hit.normal, m_PlayerRenderer.transform.TransformDirection( Vector3.forward ) );

            if( angle < 180 && angle > 150 ){
                //hit
                m_Health--;

                GameObject smoke = Instantiate(m_HitSmokePrefab, collision.transform.gameObject.transform);
                smoke.transform.position = hit.point;

                CameraShaker cameraShaker = GameObject.FindObjectOfType<CameraShaker>();
                cameraShaker.ShakeOnce(5f, 10f, 0.5f, 0.5f);

                if( !m_Invincible ){
                    if( HasAnimal() ){
                        DropAnimals();
                        MakeInvincible();
                    }else{
                        if( m_Health <= 0 ){
                            gameObject.BroadcastAll("OnPlayerHit", this);
                            SendMessageUpwards("OnPlayerDied");
                            m_EnableControls = false;
                        }else{
                            SendMessageUpwards("UpdateGUI");
                            gameObject.BroadcastAll("OnPlayerHit", this);
                            MakeInvincible();
                        }
                    }
                }
            }
        }
    }
    
    public void MakeInvincible(){
        StartCoroutine("Invincible");
    }
    
    public void OnAnimalCollected( Animal animalComponent ){
        if( HasEmptySlotForAnimal() ){
            animalComponent.m_Canvas.enabled = false;

            Transform animalPosition = m_VehicleBody.m_AnimalPosition1;
            Transform parent = m_PlayerContainer.transform;
            GameObject animal = animalComponent.gameObject;

            if( m_Animal1 == null ){
                animal.transform.SetParent(parent);
                m_Animal1 = animal;
            }else if ( m_Animal2 == null ){
                animalPosition = m_Trailer.m_AnimalPosition1;
                parent = m_Trailer.transform;
                animal.transform.SetParent(parent);
                m_Animal2 = animal;
            }else if ( m_Animal3 == null ){
                animalPosition = m_Trailer.m_AnimalPosition2;
                parent = m_Trailer.transform;
                animal.transform.SetParent(parent);
                m_Animal3 = animal;
            }
            animal.transform.position = animalPosition.position;
            animal.transform.rotation = animalPosition.rotation;

            animalComponent.SayYeah();
        
            if( m_Animal2 != null || m_Animal3 != null ){
                m_Trailer.gameObject.SetActive(true);
            }
            m_DifficultyManager.OnAnimalCollected();
        }
    }

    void StripPhysics( GameObject animal ){
        Collider collider = animal.GetComponentInChildren<Collider>();
        if( collider )
            Destroy(collider);
        Rigidbody rb = animal.GetComponentInChildren<Rigidbody>();
        if( rb != null )
            Destroy(rb);
    }

    public void OnRocketCollected(Rocket rocket){
        if( HasAnimal() ){
            Transform parent = rocket.transform;
            bool saySomething = true;
            int animalsNum = 0;
            int coins = 0;

            if( m_Animal2 != null ){
                StripPhysics(m_Animal2);
                rocket.m_Animal1 = m_Animal2;
                TransitToRocket(rocket.m_Animal1, parent, rocket.m_AnimalPosition3, saySomething);
                saySomething = false;
                m_Animal2 = null;
                animalsNum++;
                coins += animalsNum * 30;
            }

            if ( m_Animal3 != null ){
                StripPhysics(m_Animal3);
                rocket.m_Animal2 = m_Animal3;
                TransitToRocket(rocket.m_Animal2, parent, rocket.m_AnimalPosition2, saySomething);
                saySomething = false;
                m_Animal3 = null;
                animalsNum++;
                coins += animalsNum * 30;
            }

            if ( m_Animal1 != null ){
                StripPhysics(m_Animal1);
                rocket.m_Animal3 = m_Animal1;
                TransitToRocket(rocket.m_Animal3, parent, rocket.m_AnimalPosition1, saySomething);
                saySomething = false;
                m_Animal1 = null;
                animalsNum++;
                coins += animalsNum * 30;
            }

            if( m_Animal2 == null && m_Animal3 == null ){
                m_Trailer.gameObject.SetActive(false);
            }

            SendMessageUpwards("CoinCollected", coins);
            SendCoinsNum( coins, 1.5f );

            m_DifficultyManager.OnRocketCollected();
        }
    }
    
    void TransitToRocket(GameObject animal, Transform parent, Transform position, bool saySomething){
        animal.transform.SetParent(parent);
        animal.transform.position = position.position;
        animal.transform.rotation = position.rotation;
        Animal ac = animal.GetComponent<Animal>();
        GlobalGameManager.instance.IncSavedAnimal(ac.m_AnimalName);
        ac.SayYahoo();
        ac.m_Shadow.SetActive(false);
        if( saySomething ){
            animal.GetComponentInChildren<Canvas>().enabled = true;
            animal.GetComponentInChildren<Text>().text = "Yuppi!!";
        }
    }

    void TransitToGround(GameObject animal, Transform parent){
        Animal ac = animal.GetComponent<Animal>();
        animal.GetComponentInChildren<Canvas>().enabled = true;
        ac.SayNo();
        animal.transform.SetParent(parent);
        animal.GetComponentInChildren<Collider>().enabled = true;
    }

    IEnumerator Invincible () {
        float invincebleTime = 0f;
        m_Shield.SetActive(true);
        m_Invincible = true;
        while( invincebleTime < 3 ){
            invincebleTime += Time.deltaTime;
            yield return null;
        }
        m_Invincible = false;
        m_Shield.SetActive(false);


        // int oldLayer = m_PlayerRenderer.layer;
        // m_PlayerRenderer.layer = 10;
        // int index = 0;
        // MeshRenderer[] renderers = m_PlayerRenderer.GetComponentsInChildren<MeshRenderer>();
        // while( index < 16 ){
        //     foreach( MeshRenderer m in renderers ){
        //         m.enabled = !m.enabled;
        //     }
        //     yield return new WaitForSeconds(0.2f);
        //     index++;
        // }

        // m_PlayerRenderer.layer = oldLayer;
        // yield break;
    }
    
    public bool HasEmptySlotForAnimal(){
        return  m_Animal1 == null || m_Animal2 == null || m_Animal3 == null;
    }

    public bool HasAnimal(){
        return   m_Animal1 != null || m_Animal2 != null || m_Animal3 != null;
    }

    public void DropAnimals(){
        if( m_Animal2 != null ){
            TransitToGround(m_Animal2, m_DifficultyManager.m_TopLandAdditions);
            m_Animal2 = null;
        }

        if ( m_Animal3 != null ){
            TransitToGround(m_Animal3, m_DifficultyManager.m_TopLandAdditions);
            m_Animal3 = null;
        }

        if ( m_Animal1 != null ){
            TransitToGround(m_Animal1, m_DifficultyManager.m_TopLandAdditions);
            m_Animal1 = null;
        }

        m_Trailer.gameObject.SetActive(false);
    }

    public void OnSpecialAbilityActivated(){
        m_DifficultyManager.OnSpecialAbilityActivated();
    }
}
