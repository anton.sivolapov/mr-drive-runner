﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstSpecial : MonoBehaviour {
    DifficultyManager m_DifficultyManager;
    MovementController m_PlayerMovement;
    int consecutiveTapCount;
    float changeDirectionTimer;
    bool m_Enabled = true;
    float m_SpeedBonus = 30;
    float m_SpeedBonusOnEachTap = 10;

    // Use this for initialization
    void Start () {
        m_PlayerMovement = GetComponent<MovementController>();
        m_DifficultyManager = FindObjectOfType<DifficultyManager>();
    }

    public void HandleChangeDirrection(){
        if( m_Enabled ){

            float timeDelta = Time.realtimeSinceStartup - changeDirectionTimer;
            if( timeDelta > 0.2f && timeDelta < 0.5f ){
                consecutiveTapCount++;
                StartCoroutine(DecreaseSpeed());
            }

            if ( consecutiveTapCount >= 7 ){
                m_PlayerMovement.MakeInvincible();
                m_PlayerMovement.OnSpecialAbilityActivated();
            }

            changeDirectionTimer = Time.realtimeSinceStartup;
        }
    }

    IEnumerator DecreaseSpeed(){
        yield return new WaitForSeconds(2f);
        if( consecutiveTapCount > 0 )
            consecutiveTapCount--;
    }

    void Update(){
        if( m_Enabled ){
            Material mat = m_PlayerMovement.m_Renderer.material;
            mat.EnableKeyword ("_EMISSION"); 
            Color baseColor = Color.yellow;
            Color finalColor = baseColor * consecutiveTapCount/7;
            mat.SetColor ("_EmissionColor", finalColor);

            m_DifficultyManager.m_AdditionalLandSpeed = consecutiveTapCount * m_SpeedBonusOnEachTap;
        }
    }

    IEnumerator DisableEmission(){
        Material mat = m_PlayerMovement.m_Renderer.material;
        Color baseColor = Color.yellow;
        float coef = 1f;
        while( coef > 0 ){
            yield return null;
            Color finalColor = baseColor * coef;
            mat.SetColor ("_EmissionColor", finalColor);
            coef -= Time.deltaTime / 3;
        }

        m_Enabled = true;
    }
}
