﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSwitch : MonoBehaviour {
    public Image m_OnImage;
    public Image m_OffImage;
    
    public bool m_IsOn
    {
        get {
            return _on;
        }
        set {
            _on = value;
            if( _on ){
                m_OnImage.gameObject.SetActive(true);
                m_OffImage.gameObject.SetActive(false);
            }else{
                m_OnImage.gameObject.SetActive(false);
                m_OffImage.gameObject.SetActive(true);
            }
        }
    }

    private bool _on;

    // Use this for initialization
    void Start () {
        if( m_IsOn ){
            m_OnImage.gameObject.SetActive(true);
            m_OffImage.gameObject.SetActive(false);
        }else{
            m_OnImage.gameObject.SetActive(false);
            m_OffImage.gameObject.SetActive(true);
        }
    }
    
    // Update is called once per frame
    public void Toggle () {
        m_IsOn = !m_IsOn;
    }
}
