﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour {
    public int m_PlayerNum;

    private GameController m_GameController;
    private MovementController m_Player;

    void RefreshGUI(){
        m_GameController = FindObjectOfType<GameController>();

        if( m_PlayerNum == 1 ){
            m_Player = m_GameController.m_Player1;
        }else{
            m_Player = m_GameController.m_Player2;
        }

        int index = 1;
        foreach( Transform t in transform ){
            if( index > m_Player.m_Health ){
                t.gameObject.SetActive(false);
            }else{
                t.gameObject.SetActive(true);
            }
            index++;
        }
    }
}
