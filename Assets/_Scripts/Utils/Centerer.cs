﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Centerer : MonoBehaviour {

    public Vector3 m_Center;
    public bool m_DrawGizmos;

    // Use this for initialization
    void Awake () {
        Renderer[] rends = GetComponentsInChildren<Renderer>();
        if( rends.Length == 0 ) return;

        Bounds bounds = rends[0].bounds;
        foreach (Renderer rend in rends)
        {
            bounds = bounds.GrowBounds( rend.bounds );
        }
        Vector3 newPos = bounds.center;

        Transform t = transform.GetChild(0);
        t.position = -newPos;
        m_Center = bounds.center;
    }
    
    public void AlignRight(){
        foreach( Transform t in transform ){
            Vector3 newPos = t.position;
            newPos.x += m_Center.x;
            t.position = newPos;
        }
    }
    
    public void AlignLeft(){
        foreach( Transform t in transform ){
            Vector3 newPos = t.position;
            newPos.x -= m_Center.x;
            t.position = newPos;
        }
    }

    void OnDrawGizmos()
    {
        if( m_DrawGizmos ){
            Renderer[] rends = GetComponentsInChildren<Renderer>();
            if( rends.Length == 0 ) return;

            Bounds bounds = rends[0].bounds;
            foreach (Renderer rend in rends)
            {
                bounds = bounds.GrowBounds( rend.bounds );
            }
            Vector3 center = bounds.center;
            Gizmos.DrawCube( bounds.center, bounds.size );
        }
    }
}
