﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionNum : MonoBehaviour {

    void OnGUI ()
    {
        int w = Screen.width, h = Screen.height;
 
        GUIStyle style = new GUIStyle ();
        float offset = h * 2 / 100;
        Rect rect = new Rect (0, h-offset, w, offset);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color (1.0f, 1.0f, 1.0f, 1.0f);
        string text = "version " + Application.version;
        GUI.Label (rect, text, style);
    }
}
