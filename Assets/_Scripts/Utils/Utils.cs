﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BridgeDriver {
    public class Utils {
        private static System.Random rng = new System.Random();  

        public static void Shuffle<T>(IList<T> list)  
        {  
            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = rng.Next(n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }  
        }

        static public void PrettyPrintArrayInt( int[] input ){
            string res = "[";
            for( int i = 0; i < input.Length; i++ ){
                if( i == 0 ){
                    res += input[i].ToString();
                }else{
                    res += ", " + input[i].ToString();
                }
            }
            res += "]";

            Debug.Log(res);
        }
    }
}
