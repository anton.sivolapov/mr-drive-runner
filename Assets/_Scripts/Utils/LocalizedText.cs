﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour {
    public string m_Key;

    // Use this for initialization
    void Start () {
        GetComponent<Text>().text = LocalizationText.GetText(m_Key);
    }
}
