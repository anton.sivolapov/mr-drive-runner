﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

using TimeSpan = System.TimeSpan;
using AppAdvisory.SharingSystem;
#if UNITY_ANDROID
// using VoxelBusters.NativePlugins;
#endif
#if !UNITY_STANDALONE
using UnityEngine.Advertisements;
#endif

public class GameOverPopup : MonoBehaviour {
    public Button m_HomeButton;
    public Button m_ContinueButton;
    public GameObject m_ShowVideoContainer;
    public Button m_ShowVideoButton;
    public Button m_ShareButton;
    public Button m_ResetButton;
    public Button m_ShopButton;
    public GameShop m_GameShop;

    public Text m_PotionsText;
    // public Text m_TimeText;
    public Text m_ScoreText;
    public Text m_ScoreDeltaText;
    public Text m_CoinsText;
    public Text m_CoinsDeltaText;
    public Text m_DistanceText;

    public RawImage m_CharPreview;

    private GameObject m_Preview;

    void Start () {

        m_HomeButton.onClick.AddListener(OnHomeClick);
        m_ContinueButton.onClick.AddListener(OnContinueClick);
        m_ResetButton.onClick.AddListener(OnResetClick);
        m_ShowVideoButton.onClick.AddListener(OnShowVideoClick);

        m_ShareButton.onClick.AddListener(OnShareClick);
        m_ShopButton.onClick.AddListener(OnShopClick);

        // m_FBShareButton.onClick.AddListener(OnFBShareClick);
        // m_VKShareButton.onClick.AddListener(OnVKShareClick);
        // m_TwitterShareButton.onClick.AddListener(OnTwitterShareClick);

        // if( GlobalGameManager.instance.m_RussianLocale ){
        //     m_VKShareButton.gameObject.SetActive(true);
        //     m_FBShareButton.gameObject.SetActive(false);
        // }else{
        //     m_VKShareButton.gameObject.SetActive(false);
        //     m_FBShareButton.gameObject.SetActive(true);
        // }
    }
    
    void OnEnable(){
        if( Random.value > 0.8f && GlobalGameManager.instance.m_AdManager.m_RewardBasedVideoAd.IsLoaded() ){
            m_ShowVideoContainer.SetActive(false);
        }
        GlobalGameManager.instance.m_GoogleAnalytics.LogScreen("GameOverScreen");
    }
    
    void OnResetClick () {
        LoadingScreenManager.LoadSceneWithLoadingInfo(1);
    }

    void OnShowVideoClick () {
        ShowRewardedAd();
    }

    void OnHomeClick () {
        SendMessageUpwards("GoHomeScreenFromGameOver");
    }

    void OnContinueClick () {
        Debug.Log("OnContinueClick");
        if( GlobalGameManager.instance.m_Potions > 0 ){
            GlobalGameManager.instance.m_Potions--;
            ClosePopup();
            SendMessageUpwards("UsePotion");
        }else{
            m_GameShop.OpenPopup();
        }
        // gameObject.SendMessageUpwards("RestartGame");
    }

    void OnShareClick () {
        VSSHARE.OnScreenshotTaken += OnScreenshotTakenDelegate;
        VSSHARE.DOTakeScreenShot();

    }

    void OnShopClick () {
        m_GameShop.OpenPopup();
    }
    
    void OnScreenshotTakenDelegate(Texture2D tex)
    {
        VSSHARE.OnScreenshotTaken -= OnScreenshotTakenDelegate;
        Debug.Log("UnityEventListener - Screenshot taken!!");
        // VSSHARE.DOOpenScreenshotButton();
        VSSHARE.DOShareScreenshot(LocalizationText.GetText("twitter_text"), ShareType.Native);

        Analytics.CustomEvent("shareScreenshot", new Dictionary<string, object>
                {
                    { "shared", true } 
                });
    }

    public void OpenPopup(){
        Refresh();
        gameObject.SetActive(true);
    }

    public void Refresh(){
        if( GlobalGameManager.instance.m_Potions <= 0 ){
            m_ShopButton.gameObject.SetActive(true);
            m_ContinueButton.gameObject.SetActive(false);
        }else{
            m_ShopButton.gameObject.SetActive(false);
            m_ContinueButton.gameObject.SetActive(true);
        }

        GameController gc = GetComponentInParent<GameController>();

        string score = string.Format("{0:n0}", gc.m_CurrentScore );
        string scoreDelta = string.Format("+{0:n0}", gc.m_ScoreDelta );
        m_ScoreText.text = score;
        m_ScoreDeltaText.text = scoreDelta;

        string coins = string.Format( "{0:n0}", gc.m_CurrentCoins );
        string coinsDelta = string.Format( "+{0:n0}", gc.m_CoinsDelta );
        m_CoinsText.text = coins;
        m_CoinsDeltaText.text = coinsDelta;

        string potions = GlobalGameManager.instance.m_Potions.ToString();
        m_PotionsText.text = potions;

        string distance = string.Format("{0:n0}M", gc.m_CurrentDistance);
        string distanceDelta = string.Format("+{0:n0}", gc.m_DeltaDistance);
        m_DistanceText.text = distance;

        if( m_Preview == null ){
            m_Preview = Instantiate( GlobalGameManager.instance.m_SelectedVehicle.m_Preview );
            Vector3 origPos = m_Preview.transform.position;
            origPos.x = 1000f;
            m_Preview.transform.position = origPos;

            // Camera camera = m_Preview.GetComponentInChildren<Camera>();
            // m_CharPreview.texture = camera.targetTexture;
        }
    }
    
    public void ClosePopup(){
        gameObject.SetActive(false);
    }

#if !UNITY_STANDALONE
    void ShowRewardedAd()
    {
        GlobalGameManager.instance.m_AdManager.ShowRewardBasedAd( RewardAfterVideo, VideoSkiped );


        // if (Advertisement.IsReady("rewardedVideo"))
        // {
        //     Debug.Log("Ads is ready");
        //     var options = new ShowOptions { resultCallback = HandleShowResult };
        //     Advertisement.Show("rewardedVideo", options);
        // }else{
        //     Debug.Log("Ads is NOT ready");
        //     GlobalGameManager.instance.Notify(LocalizationText.GetText("ads_not_ready", 500));
        // }
    }

    private void VideoSkiped(){
        m_ShowVideoContainer.SetActive(false);
    }

    private void RewardAfterVideo()
    {
        m_ShowVideoContainer.SetActive(false);
        Debug.Log("The ad was successfully shown.");

        int reward = Random.Range(100, 700);
        GlobalGameManager.instance.m_Coins += reward;

        GlobalGameManager.instance.Notify( string.Format(LocalizationText.GetText("rewarded_video_notification"), reward));

        Analytics.CustomEvent("rewardedVideoWatchedInGame", new Dictionary<string, object>
                {
                    { "continue", true }
                });
        /**
        switch (result)
        {
            case ShowResult.Finished:
                
                ClosePopup();
                SendMessageUpwards("UsePotion");
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                GlobalGameManager.instance.Notify(LocalizationText.GetText("ad_skiped_lbl"));
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                GlobalGameManager.instance.Notify(LocalizationText.GetText("noads_lbl"));
                break;
        }
        **/
    }
#endif
}
