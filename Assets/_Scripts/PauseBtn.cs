﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseBtn : EventTrigger {
    public override void OnPointerClick( PointerEventData data )
    {
        gameObject.SendMessageUpwards("OnPauseClick");
    }
}
