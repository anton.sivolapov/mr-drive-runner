﻿using UnityEngine;
using System.Collections;
#if !UNITY_STANDALONE
using UnityEngine.Advertisements;
#endif
 
public class UnityShowAds: MonoBehaviour
{
#if !UNITY_STANDALONE
    void Start(){
        StartCoroutine(ShowAds());
    }

    public IEnumerator ShowAds()
    {
        Debug.Log("showads start");
        while (!Advertisement.IsReady()){
            Debug.Log("showads");
            yield return null;
        }
 
        Advertisement.Show ();
    }
#endif
}
