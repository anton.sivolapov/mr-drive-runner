﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnScreenControll : MonoBehaviour {
    public int m_PlayerNum;

    private MovementController m_PlayerMovement;
    private bool m_GoLeft;
    private bool m_GoRight;
    private string m_PrevDirection;

    private int tapCount;
    private float doubleTapTimer;
    
    private float m_SuperPowerFillRate;

    private GameController m_GameController;

    // Use this for initialization
    void Start () {
        // SwipeManager swipeManager = FindObjectOfType<SwipeManager>();
        // swipeManager.onSwipe += HandleSwipe;

        m_GameController = FindObjectOfType<GameController>();

        if( m_PlayerNum == 1 )
            m_PlayerMovement = m_GameController.m_Player1;

        if( m_PlayerNum == 2 )
            m_PlayerMovement = m_GameController.m_Player2;
    }
    
    public void HandleSwipe(SwipeAction swipeAction){
        var rectTransform = GetComponent<RectTransform>();
        float width = rectTransform.rect.width;
        float height = rectTransform.rect.height;
    }

    public void GoLeft(){
        m_GoLeft = true;
    }

    public void GoRight(){
        m_GoRight = true;
    }

    public void StopRight(){
        m_GoRight = false;
    }
    public void StopLeft(){
        m_GoLeft = false;
    }

    public void ChangeDirrection(){
        tapCount++;
        m_PlayerMovement.m_BurstSpecial.HandleChangeDirrection();


        if (tapCount > 0){
            doubleTapTimer += Time.deltaTime;
        }
        if (doubleTapTimer > 0.2f){
            doubleTapTimer = 0f;
            tapCount = 0;
        }
        if (tapCount >= 1){
            //What you want to do
            doubleTapTimer = 0.0f;
            tapCount = 0;

            if( m_GoLeft ){
                m_PrevDirection = "left";
                m_GoRight = true;
                m_GoLeft = false;
            }else if ( m_GoRight ){
                m_PrevDirection = "right";
                m_GoRight = false;
                m_GoLeft = true;
            }else{
                //do nothing
                if( m_PrevDirection == "left" ){
                    m_GoRight = false;
                    m_GoLeft = true;
                }else{
                    m_GoRight = true;
                    m_GoLeft = false;
                }
            }
        }

    }

    void Update () {
        if( m_PlayerMovement == null ){
            if( m_PlayerNum == 1 )
                m_PlayerMovement = m_GameController.m_Player1;

            if( m_PlayerNum == 2 )
                m_PlayerMovement = m_GameController.m_Player2;
        }

        if( m_GoLeft ){
            m_PlayerMovement.GoLeft();
        }else if( m_GoRight ){
            m_PlayerMovement.GoRight();
        }else{
            m_PlayerMovement.GoStraight();
        }

        // if (tapCount > 0){
        //     doubleTapTimer += Time.deltaTime;
        // }

        // if (consecutiveTapCount > 0){
        //     changeDirectionTimer += Time.deltaTime;
        // }

        // if (tapCount >= 2){
        //     //What you want to do
        //     doubleTapTimer = 0.0f;
        //     tapCount = 0;
        //     Debug.Log("================= doubletap!!!!!!!");

        //     m_GoRight = false;
        //     m_GoLeft = false;
        // }
        // if (doubleTapTimer > 0.2f){
        //     doubleTapTimer = 0f;
        //     tapCount = 0;
        // }


        // if ( consecutiveTapCount >= 5 ){
        //     //10% in one second
        //     Debug.Log("super ability activated");
        // }
        // if (changeDirectionTimer > 0.5f){
        //     Debug.Log("super ability canceled");
        //     consecutiveTapCount = 0;
        //     changeDirectionTimer = 0f;
        // }
    }

    // Update is called once per frame
    void OldUpdate () {
        if( m_GoLeft ){
            m_PlayerMovement.GoLeft();
        }else if( m_GoRight ){
            m_PlayerMovement.GoRight();
        }else{
            m_PlayerMovement.GoStraight();
        }

        // if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began){
        //     tapCount++;
        // }
        // if (tapCount > 0){
        //     doubleTapTimer += Time.deltaTime;
        // }

        // if (tapCount >= 2){
        //     //What you want to do
        //     doubleTapTimer = 0.0f;
        //     tapCount = 0;
        //     Debug.Log("================= doubletap!!!!!!!");
        //     // m_PlayerMovement.m_Special.Activate();
        // }
        // if (doubleTapTimer > 0.2f){
        //     doubleTapTimer = 0f;
        //     tapCount = 0;
        // }
    }
}
