﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAbility : MonoBehaviour {

    private SpecialAbilityCenter m_SpecialAbilityCenter;

    void Awake(){
        m_SpecialAbilityCenter = FindObjectOfType<SpecialAbilityCenter>();
    }

    public virtual void Activate(){
    }

    public virtual void OnAbilityReady(){
        m_SpecialAbilityCenter.StartPulsation();
    }

    public virtual void OnAbilityActivated(){
        m_SpecialAbilityCenter.StopPulsation();
    }
}
