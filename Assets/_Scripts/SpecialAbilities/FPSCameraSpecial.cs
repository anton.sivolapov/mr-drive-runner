﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCameraSpecial : SpecialAbility {
    private GameObject m_FPSCamera;
    private GameObject m_ThirdPartyCamera;
    private bool m_IsReady = true;
    private MovementController m_MovementController;
      
    void Start(){
        m_MovementController = GetComponent<MovementController>();
        m_FPSCamera = GameObject.Find("CameraRigFirstPerson").transform.GetChild(0).gameObject;
        m_ThirdPartyCamera = GameObject.Find("CameraRig").transform.GetChild(0).gameObject;
    }

    public override void Activate(){
        if( m_IsReady ){
            OnAbilityActivated();
            m_IsReady = false;

            m_MovementController.m_CoinsMagnet.SetActive(true);
            m_FPSCamera.SetActive(true);
            m_ThirdPartyCamera.SetActive(false);

            StartCoroutine(Cooldown());
        }
    }

    IEnumerator Cooldown(){
        yield return new WaitForSeconds( 20f );

        m_MovementController.m_CoinsMagnet.SetActive(false);
        m_FPSCamera.SetActive(false);
        m_ThirdPartyCamera.SetActive(true);

        yield return new WaitForSeconds( 20f );
        m_IsReady = true;
        OnAbilityReady();
    }
}
