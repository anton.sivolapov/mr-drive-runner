﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallerSpecial : SpecialAbility {
    private MovementController m_MovementController;
    private bool m_IsReady = true;
      
    void Start(){
        m_MovementController = GetComponent<MovementController>();
    }

    public override void Activate(){
        if( m_IsReady ){
            OnAbilityActivated();
            m_IsReady = false;
            m_MovementController.m_PlayerContainer.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            StartCoroutine(Cooldown());
        }
    }

    IEnumerator Cooldown(){
        yield return new WaitForSeconds( 20f );
        m_MovementController.m_PlayerContainer.transform.localScale = Vector3.one;
        yield return new WaitForSeconds( 20f );
        m_IsReady = true;
        OnAbilityReady();
    }
}
