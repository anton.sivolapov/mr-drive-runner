﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class Trailer : MonoBehaviour {
    public GameObject m_ObjectPoint;
    public float m_Distant;

    public Transform m_AnimalPosition1;
    public Transform m_AnimalPosition2;
    
    private Vector3 m_PrevPosition;

    void Update () {
        Vector3 pos = m_ObjectPoint.transform.position;
        // pos.x = (float)Math.Round( pos.x, 1 );
        pos.x = Mathf.Round( pos.x );

        bool stop = false;
        
        transform.LookAt( m_ObjectPoint.transform );
        Vector3 velocity = Vector3.zero;
        float dampTime = 0.1f;
        
        if( m_PrevPosition != Vector3.zero ){
            if( m_PrevPosition == pos ){
                stop = true;
            }
        }
        m_PrevPosition = pos;


        if (stop){
            Vector3 position = transform.position;
            Vector3 b = m_ObjectPoint.transform.position - new Vector3(position.x, m_ObjectPoint.transform.position.y, m_ObjectPoint.transform.position.z);
            Vector3 target = transform.position + b;
            transform.position = Vector3.SmoothDamp(transform.position, target, ref velocity, dampTime);
        }

        if (transform.position.x - m_ObjectPoint.transform.position.x > m_Distant){
            float x = m_ObjectPoint.transform.position.x + m_Distant;
            Vector3 position2 = transform.position;
            position2.x = x;
            transform.position = position2;
        } else if (transform.position.x - m_ObjectPoint.transform.position.x < -m_Distant){
            float x2 = m_ObjectPoint.transform.position.x - m_Distant;
            Vector3 position3 = transform.position;
            position3.x = x2;
            transform.position = position3;
        }
    }
}
