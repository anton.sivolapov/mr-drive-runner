﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorsField : MonoBehaviour {

    // Use this for initialization
    void Start () {
        StartCoroutine(RandomizeRotation());
    }
    
    IEnumerator RandomizeRotation(){
        while( true ){
            yield return new WaitForSeconds( Random.Range( 15f, 45f ) );
            Vector3 newAngles = new Vector3(0, Random.Range( 0, 45f ), 0);
            transform.rotation = Quaternion.Euler(newAngles);
        }
    }
}
