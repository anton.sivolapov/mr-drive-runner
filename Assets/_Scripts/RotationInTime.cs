﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationInTime : MonoBehaviour {
    public Vector3 m_SpinSpeed;
    public bool m_ContinueOnPause;

    void Start(){
        StartCoroutine("RotateWorld");
    }

    IEnumerator RotateWorld(){
        while( true ){
            while( !m_ContinueOnPause && GlobalGameManager.paused ){
                yield return null;
            }
            Quaternion deltaRotation = Quaternion.Euler(m_SpinSpeed * Time.deltaTime);
            transform.rotation = transform.rotation * deltaRotation;
            yield return null;
        }
    }
}
