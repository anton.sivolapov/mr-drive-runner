﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePlayer : MonoBehaviour {
    MovementController m_MovementController;
    void Awake(){
        m_MovementController = GetComponentInParent<MovementController>();
    }
    // void OnCollisionEnter(Collision collision) {
    //     Debug.Log("OnCollisionEnter: " + collision.transform.gameObject.name);
    //     m_MovementController.OnBodyCollisionEnter(collision);
    // }

    void OnTriggerEnter(Collider other) {
        m_MovementController.OnBodyCollisionEnter(other);
    }
}
