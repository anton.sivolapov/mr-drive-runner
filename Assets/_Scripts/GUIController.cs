﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {
    public GameObject m_PausePopup;
    public GameObject m_GameOverPopup;

    [Header("Controls")]
    public GameObject m_OnePlayerGUI;
    public GameObject m_MultiplayerGUI;

    [Header("Stats")]
    public Text m_CoinsText;
    public Text m_ScoreText;
    public Text m_TimeText;
    public Text m_CurrentDistanceText;
    public Text m_TopDistanceText;

    private GameController m_GameController;
     
    // Use this for initialization
    void Start () {
        m_GameController = FindObjectOfType<GameController>();
        if( m_GameController.m_TwoPlayersGame ){
            m_OnePlayerGUI.SetActive(false);
            m_MultiplayerGUI.SetActive(true);
        }else{
            m_OnePlayerGUI.SetActive(true);
            m_MultiplayerGUI.SetActive(false);
        }

        StartCoroutine(UpdateStats());
    }
    
    public void AddCoins ( string coins ) {
        m_CoinsText.text = coins;
    }
    
    public void AddScore ( string score ) {
        m_ScoreText.text = score;
    }

    public void AddTime ( string time ) {
        m_TimeText.text = time;
    }

    void OnPauseClick () {
        gameObject.SendMessageUpwards("PauseGame");
        m_PausePopup.SetActive(true);
    }

    void ResumeGame () {
        m_PausePopup.SetActive(false);
    }

    public void ShowGameOverPopup () {
        m_GameOverPopup.GetComponent<GameOverPopup>().OpenPopup();
    }


    public void RefreshAllUI(){
        m_GameOverPopup.GetComponent<GameOverPopup>().m_PotionsText.text = GlobalGameManager.instance.m_Potions.ToString();
    }

    IEnumerator UpdateStats(){
        float currentTop = GlobalGameManager.instance.m_TopDistance;
        m_TopDistanceText.text = string.Format("BEST: {0:n0}M", currentTop);

        while(true){
            while( GlobalGameManager.paused ){
                yield return null;
            }

            m_CoinsText.text = string.Format("{0:n0}", m_GameController.m_CurrentCoins);

            float currentDistance = m_GameController.m_CurrentDistance;
            m_CurrentDistanceText.text = string.Format("{0:n0}M", currentDistance);

            if( currentTop <  currentDistance ){
                m_TopDistanceText.text = string.Format("BEST: {0:n0}M", currentDistance);
            }
            yield return null;
        }
    }
}
