﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour {
    public GameObject m_Explosion;

    // Use this for initialization
    void Start () {
        Invoke("DestroySelf", 10f);
    }
    
    // Update is called once per frame
    void Update () {
        
    }

    void OnCollisionEnter(){
        GameObject explosion = Instantiate(m_Explosion);
        Vector3 newPos = transform.position;
        newPos.y = 0f;
        explosion.transform.position = newPos;
        GetComponentInChildren<Projector>().enabled = false;
        Invoke("DestroySelf", 0.3f);

    }

    void DestroySelf(){
        Destroy(gameObject);
    }
}
