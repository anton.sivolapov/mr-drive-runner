﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialAbilityCenter : MonoBehaviour {

    public Button m_SpecialAbilityButton;
    // Use this for initialization
    void Start () {
        m_SpecialAbilityButton = FindObjectOfType<SpecialAbilityButton>().GetComponent<Button>();
        StartPulsation();
    }
    
    public void StartPulsation(){
        Vector3 targetScale = transform.localScale * 1.05f;
        iTween.ScaleTo(gameObject, iTween.Hash("scale", targetScale, "easeType", "easeInOutQuad", "time", 0.1f, "loopType", "pingPong" ));

        m_SpecialAbilityButton.gameObject.SetActive(true);

        // ColorBlock cb = m_Btn.colors;
        // cb.normalColor = new Color(1,1,1,0.5f);
        // m_Btn.colors = cb;
    }

    public void StopPulsation(){
        iTween.Stop(gameObject);

        // Vector3 targetScale = transform.localScale * 1.05f;
        // iTween.ScaleTo(gameObject, iTween.Hash("scale", targetScale, "easeType", "easeInOutQuad", "time", 0.1f, "loopType", "pingPong" ));

        m_SpecialAbilityButton.gameObject.SetActive(false);

        // ColorBlock cb = m_Btn.colors;
        // cb.normalColor = new Color(1,1,1,1);
        // m_Btn.colors = cb;
    }
}
