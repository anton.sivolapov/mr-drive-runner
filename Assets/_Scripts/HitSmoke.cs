﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitSmoke : MonoBehaviour {
    // Use this for initialization
    void Start () {
        Invoke("DestroySmoke", 3);
    }

    void DestroySmoke(){
        Destroy(gameObject);
    }
}
