﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Animal : MonoBehaviour {
    public GameObject m_AnimalPrefab;
    public GameObject m_Shadow;
    
    public AudioSource m_AudioSource;
    public AudioClip m_HelpSound;
    public AudioClip m_NoSound;
    public AudioClip m_YahooSound;
    public AudioClip m_YeahSound;
    public string m_AnimalName;
    public BoxCollider m_Collider;
    public Canvas m_Canvas;
    
    void Awake(){
        m_Canvas = GetComponentInChildren<Canvas>();
        m_AudioSource = GetComponent<AudioSource>();
        m_Collider = GetComponentInChildren<BoxCollider>();
    }

    void OnTriggerEnter(Collider other) {
        if( other.gameObject.tag == "Player" ){
            other.gameObject.GetComponentInParent<MovementController>().OnAnimalCollected(this);
            // Destroy(gameObject);
        }else if( other.gameObject.tag == "BehindPlayer" ){
            SayNo();
        }
    }
    
    public void SayNo(){
        GetComponentInChildren<Text>().text = "Nooo!!";
        m_AudioSource.PlayOneShot(m_NoSound);
    }

    public void SayHelp(){
        m_AudioSource.PlayOneShot(m_HelpSound);
    }

    public void SayYahoo(){
        m_AudioSource.PlayOneShot(m_YahooSound);
    }

    public void SayYeah(){
        m_AudioSource.PlayOneShot(m_YeahSound);
    }
}
