﻿using UnityEngine;
using Guid = System.Guid;
using System.Collections;
using System.Collections.Generic;       //Allows us to use Lists. 

using SerializerFree;
using SerializerFree.Serializers;

#if !UNITY_STANDALONE
using UnityEngine.Advertisements;
#endif

#if UNITY_ANDROID
// using VoxelBusters.NativePlugins;
#endif

using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using AppAdvisory.VSRATE;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

[System.Serializable]
public class InfosCollection
{
    public bool[] vehicles;
}   

public class GlobalGameManager : MonoBehaviour
{
    //Static instance of GameManager which allows it to be accessed by any other script.
    public static GlobalGameManager instance = null;              
    public static bool paused = false;              
    
    public GameData m_GameData;
    // public int m_SelectedVehicle;

    public Dictionary<string, int> m_SavedAnimals;

    public Dictionary<string, int> m_NonSavedAnimals;
    public AdManager m_AdManager;

    //represents opened vehicled by index
    public bool[] m_OpenedVehicles;
    public Vehicle m_SelectedVehicle;
    public GameObject m_NotificationPrefab;
    public bool m_IsMultiplayerGame;
    public bool m_RussianLocale;

    public bool m_UseObjectPool = false;

    [Header("Google Analytics")]
    [HideInInspector]
    public GoogleAnalyticsV4 m_GoogleAnalytics;
    public GameObject m_GoogleAnalyticsPrefab;

    //rate us
    public bool m_GameWasRated
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.gameWasRated") ){
                PlayerPrefs.SetInt("BridgeDriver.gameWasRated", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.gameWasRated") == 1;
        }
        set {
            if( value ){
                PlayerPrefs.SetInt("BridgeDriver.gameWasRated", 1);
            }else{
                PlayerPrefs.SetInt("BridgeDriver.gameWasRated", 0);
            }
        }
    }

    public int m_NumberOfRuns
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.numberOfRuns") ){
                PlayerPrefs.SetInt("BridgeDriver.numberOfRuns", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.numberOfRuns");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.numberOfRuns", value);
        }
    }

    public bool m_SoundEnabled
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.soundEnabled") ){
                PlayerPrefs.SetInt("BridgeDriver.soundEnabled", 1);
            }
            return PlayerPrefs.GetInt("BridgeDriver.soundEnabled") == 1;
        }
        set {
            if( value ){
                PlayerPrefs.SetInt("BridgeDriver.soundEnabled", 1);
            }else{
                PlayerPrefs.SetInt("BridgeDriver.soundEnabled", 0);
            }
            UpdateSoundConfig();
        }
    }

    public bool m_DoubleCoinsPurchased
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.doubleCoinsPurchased") ){
                PlayerPrefs.SetInt("BridgeDriver.doubleCoinsPurchased", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.doubleCoinsPurchased") == 1;
        }
        set {
            if( value ){
                PlayerPrefs.SetInt("BridgeDriver.doubleCoinsPurchased", 1);
            }else{
                PlayerPrefs.SetInt("BridgeDriver.doubleCoinsPurchased", 0);
            }
        }
    }

    public bool m_AdsRemoverPurchased
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.adsRemoverPurchased") ){
                PlayerPrefs.SetInt("BridgeDriver.adsRemoverPurchased", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.adsRemoverPurchased") == 1;
        }
        set {
            if( value ){
                PlayerPrefs.SetInt("BridgeDriver.adsRemoverPurchased", 1);
            }else{
                PlayerPrefs.SetInt("BridgeDriver.adsRemoverPurchased", 0);
            }
        }
    }

    public bool m_StarterPackAvailable
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.starterPackAvailable") ){
                PlayerPrefs.SetInt("BridgeDriver.starterPackAvailable", 1);
            }
            return PlayerPrefs.GetInt("BridgeDriver.starterPackAvailable") == 1;
        }
        set {
            if( value ){
                PlayerPrefs.SetInt("BridgeDriver.starterPackAvailable", 1);
            }else{
                PlayerPrefs.SetInt("BridgeDriver.starterPackAvailable", 0);
            }
        }
    }

    public int m_SelectedVehicleIndex
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.selecledVehicle") ){
                PlayerPrefs.SetInt("BridgeDriver.selecledVehicle", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.selecledVehicle");
        }
        set {
            m_SelectedVehicle = m_GameData.m_Vehicles[value];
            PlayerPrefs.SetInt("BridgeDriver.selecledVehicle", value);
        }
    }

    public int m_NumberOfGameSessions
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.numberOfGameSessions") ){
                PlayerPrefs.SetInt("BridgeDriver.numberOfGameSessions", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.numberOfGameSessions");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.numberOfGameSessions", value);
        }
    }

    public float m_LastDistance
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.lastDistance") ){
                PlayerPrefs.SetFloat("BridgeDriver.lastDistance", 0);
            }
            return PlayerPrefs.GetFloat("BridgeDriver.lastDistance");
        }
        set {
            PlayerPrefs.SetFloat("BridgeDriver.lastDistance", value);
        }
    }

    public float m_TotalDistance
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.totalDistance") ){
                PlayerPrefs.SetFloat("BridgeDriver.totalDistance", 0);
            }
            return PlayerPrefs.GetFloat("BridgeDriver.totalDistance");
        }
        set {
            PlayerPrefs.SetFloat("BridgeDriver.totalDistance", value);
        }
    }

    public float m_TopDistance
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.topDistance") ){
                PlayerPrefs.SetFloat("BridgeDriver.topDistance", 0);
            }
            return PlayerPrefs.GetFloat("BridgeDriver.topDistance");
        }
        set {
            PlayerPrefs.SetFloat("BridgeDriver.topDistance", value);
        }
    }

    public int m_Coins
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.coins") ){
                PlayerPrefs.SetInt("BridgeDriver.coins", m_GameData.m_InitCoins);
            }
            return PlayerPrefs.GetInt("BridgeDriver.coins");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.coins", value);
        }
    }

    public int m_TotalCoins
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.total_coins") ){
                PlayerPrefs.SetInt("BridgeDriver.total_coins", m_GameData.m_InitCoins);
            }
            return PlayerPrefs.GetInt("BridgeDriver.total_coins");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.total_coins", value);
        }
    }

    public int m_Potions
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.potions") ){
                PlayerPrefs.SetInt("BridgeDriver.potions", m_GameData.m_InitPotions);
            }
            return PlayerPrefs.GetInt("BridgeDriver.potions");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.potions", value);
        }
    }
    
    public int m_Score
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.score") ){
                PlayerPrefs.SetInt("BridgeDriver.score", m_GameData.m_InitScore);
            }
            return PlayerPrefs.GetInt("BridgeDriver.score");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.score", value);
        }
    }
    
    public int m_TransactionCounter
    {
        get {
            if( !PlayerPrefs.HasKey("BridgeDriver.transactionCounter") ){
                PlayerPrefs.SetInt("BridgeDriver.transactionCounter", 0);
            }
            return PlayerPrefs.GetInt("BridgeDriver.transactionCounter");
        }
        set {
            PlayerPrefs.SetInt("BridgeDriver.transactionCounter", value);
        }
    }

    public string m_InstallationID
    {
        get {
            string unique = string.Format("{0}_{1:N}", Random.Range(10000000, 1000000000), Guid.NewGuid());

            if( !PlayerPrefs.HasKey("BridgeDriver.installationID") ){
                PlayerPrefs.SetString("BridgeDriver.installationID", unique);
            }
            return PlayerPrefs.GetString("BridgeDriver.installationID");
        }
    }

    public  string m_GoogleId = "1342652";

    private GameObject m_CurrentNotification;

    private bool m_isGameServiceAvailable;
    // private string m_GlobalLeaderboardId = "top_players_score_leaderboard";
    // private string m_GlobalDistanceLeaderboardId = "top_players_distance_leaderboard";
    // private string m_GlobalMultiplayerLeaderboardId = "top_players_multiplayer_leaderboard";

    //Awake is always called before any Start functions
    void Awake()
    {
            
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern,
            // meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);    
            
        m_GoogleAnalytics = GameObject.FindObjectOfType<GoogleAnalyticsV4>();
        if( m_GoogleAnalytics == null ){
            m_GoogleAnalytics = Instantiate(m_GoogleAnalyticsPrefab).GetComponent<GoogleAnalyticsV4>();
        }

        m_GoogleAnalytics.StartSession();

        m_NumberOfRuns++;
        m_AdManager = GetComponent<AdManager>();
        if( !PlayerPrefs.HasKey("BridgeDriver.openedVehicles") ){
            InfosCollection i = new InfosCollection();
            i.vehicles = new bool[m_GameData.m_Vehicles.Length];
            i.vehicles[0] = true;
            string y = JsonUtility.ToJson(i);

            PlayerPrefs.SetString("BridgeDriver.openedVehicles", y);
        }
        InfosCollection ic = JsonUtility.FromJson<InfosCollection>( PlayerPrefs.GetString("BridgeDriver.openedVehicles") );
        m_OpenedVehicles = ic.vehicles;
        
        m_SelectedVehicle = m_GameData.m_Vehicles[m_SelectedVehicleIndex];

        if( Application.systemLanguage == SystemLanguage.Russian || 
            Application.systemLanguage == SystemLanguage.Ukrainian ||
            Application.systemLanguage == SystemLanguage.Belarusian ){
            LocalizationText.SetLanguage("RU");
            m_RussianLocale = true;
        }else{
            LocalizationText.SetLanguage("EN");
            m_RussianLocale = false;
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
        
    }

    
    void Start(){

        // Debug.Log(GetSavedAnimal("cat"));
        // IncSavedAnimal("cat");
        // Debug.Log(GetNonSavedAnimal("dog"));
        // IncNonSavedAnimal("dog");
        
        #if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            // enables saving game progress.
            // .EnableSavedGames()
            // registers a callback to handle game invitations received while the game is not running.
            // .WithInvitationDelegate(<callback method>)
            // registers a callback for turn based match notifications received while the
            // game is not running.
            // .WithMatchDelegate(<callback method>)
            // requests the email address of the player be available.
            // Will bring up a prompt for consent.
            // .RequestEmail()
            // requests a server auth code be generated so it can be passed to an
            //  associated back end server application and exchanged for an OAuth token.
            .RequestServerAuthCode(false)
            // requests an ID token be generated.  This OAuth token can be used to
            //  identify the player to other services such as Firebase.
            .RequestIdToken()
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        #endif

        Social.localUser.Authenticate((bool success) => {
            // handle success or failure
        });

        UpdateSoundConfig();

#if UNITY_ANDROID
        // Advertisement.Initialize( m_GoogleId );

        // //NativePlugins
        // m_isGameServiceAvailable = NPBinding.GameServices.IsAvailable();

        // if( m_isGameServiceAvailable ){
        //     if( !NPBinding.GameServices.LocalUser.IsAuthenticated ){
        //         NPBinding.GameServices.LocalUser.Authenticate((bool _success, string _error)=>{
        //                 if (_success)
        //                 {
        //                     Debug.Log("Sign-In Successfully");
        //                     Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser.ToString());
        //                 }
        //                 else
        //                 {
        //                     Debug.Log("Sign-In Failed with error " + _error);
        //                 }
        //             });
        //     }
        // }

        // Scene scene = SceneManager.GetActiveScene();

        Invoke( "ShowRateUs", 1f );
#endif
    }
    
    void InitNonSavedAnimal(){
        Dictionary<string, int> data = new Dictionary<string, int>();

        if( !PlayerPrefs.HasKey("BridgeDriver.nonsavedAnimals") ){
            PlayerPrefs.SetString("BridgeDriver.nonsavedAnimals", Serializer.Serialize ( data ));
            m_NonSavedAnimals = data;
        }
        if( m_NonSavedAnimals == null ){
            string tmp = PlayerPrefs.GetString("BridgeDriver.nonsavedAnimals");
            m_NonSavedAnimals = Serializer.Deserialize<Dictionary<string, int>> ( tmp );
        }
    }
    
    public void IncNonSavedAnimal( string name ){
        InitNonSavedAnimal();

        if( !m_NonSavedAnimals.ContainsKey(name) ){
            m_NonSavedAnimals.Add(name, 1);
        }else{
            m_NonSavedAnimals[name]++;
        }

        PlayerPrefs.SetString("BridgeDriver.nonsavedAnimals", Serializer.Serialize ( m_NonSavedAnimals ));
    }

    int GetNonSavedAnimal( string name ){
        InitNonSavedAnimal();

        if( !m_NonSavedAnimals.ContainsKey(name) ){
            return 0;
        }
        return m_NonSavedAnimals[name];
    }

    void InitSavedAnimal(){
        Dictionary<string, int> data = new Dictionary<string, int>();
        if( !PlayerPrefs.HasKey("BridgeDriver.savedAnimals") ){
            PlayerPrefs.SetString("BridgeDriver.savedAnimals", Serializer.Serialize ( data ));
            m_SavedAnimals = data;
        }
        if( m_SavedAnimals == null ){
            string tmp = PlayerPrefs.GetString("BridgeDriver.savedAnimals");
            m_SavedAnimals = Serializer.Deserialize<Dictionary<string, int>> ( tmp );
        }
    }

    public void IncSavedAnimal( string name ){
        InitSavedAnimal();

        if( !m_SavedAnimals.ContainsKey(name) ){
            m_SavedAnimals.Add(name, 1);
        }else{
            m_SavedAnimals[name]++;
        }

        PlayerPrefs.SetString("BridgeDriver.savedAnimals", Serializer.Serialize ( m_SavedAnimals ));
    }

    int GetSavedAnimal( string name ){
        InitSavedAnimal();
        
        if( !m_SavedAnimals.ContainsKey(name) ){
            return 0;
        }
        return m_SavedAnimals[name];
    }

    void ShowRateUs(){
        if( !m_GameWasRated && m_NumberOfRuns % 3 == 0 ){
        // if( true ){
            RateUsManager.ShowRateUsWindows();
            // RateUsManager.SetStars(9);
        }
    }
    
    void UpdateSoundConfig(){
        if( m_SoundEnabled ){
            AudioListener.pause = false;
        }else{
            AudioListener.pause = true;
        }
    }
    
    public void OpenVehicle( int index ){
        InfosCollection ic = JsonUtility.FromJson<InfosCollection>( PlayerPrefs.GetString("BridgeDriver.openedVehicles") );
        ic.vehicles[index] = true;
        m_OpenedVehicles = ic.vehicles;
        m_Coins -= m_GameData.m_Vehicles[index].m_Price ;

        string y = JsonUtility.ToJson(ic);
        PlayerPrefs.SetString("BridgeDriver.openedVehicles", y);

        Analytics.CustomEvent("unlockVehicle", new Dictionary<string, object>
                              {
                                  { "vehicle", m_SelectedVehicle.m_Renderer.name }
                              });

        #if UNITY_ANDROID
        if( m_SelectedVehicle.m_Renderer.name == "FighterJet" ){
            // NPBinding.GameServices.ReportProgressWithGlobalID("ace", 1.0, null);
        }
        if( m_SelectedVehicle.m_Renderer.name == "UFO" ){
            // NPBinding.GameServices.ReportProgressWithGlobalID("ufo", 1.0, null);
        }
        #endif
    }
        
    public void SetScore( int score ){
        PlayerPrefs.SetInt("BridgeDriver.score", score);
    }

    public void Notify( string text ){
        if( m_CurrentNotification == null ){
            m_CurrentNotification = Instantiate(m_NotificationPrefab);
        }
        m_CurrentNotification.GetComponent<SmallNotification>().ShowNotification(text, 3);
    }

    public void ShowLeaderboard(){

        Social.localUser.Authenticate((bool success) => {
                Social.ShowLeaderboardUI();
                // handle success or failure
            });

#if UNITY_ANDROID
        // if( m_isGameServiceAvailable ){

        //     if( !NPBinding.GameServices.LocalUser.IsAuthenticated ){
        //         Debug.Log("=== user not authenticated, prepare auth first");
        //         NPBinding.GameServices.LocalUser.Authenticate((bool _success, string _error)=>{
        //                 if (_success)
        //                 {
        //                     Debug.Log("Sign-In Successfully");
        //                     Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser.ToString());

        //                     NPBinding.GameServices.ShowLeaderboardUIWithGlobalID(null, eLeaderboardTimeScope.ALL_TIME, (string _lerror)=>{
        //                             Debug.Log("Closed leaderboard UI. Error: " + _lerror);
        //                         });
        //                 }
        //                 else
        //                 {
        //                     Debug.Log("Sign-In Failed with error " + _error);
        //                 }
        //             });
        //     }else{
        //         NPBinding.GameServices.ShowLeaderboardUIWithGlobalID(null, eLeaderboardTimeScope.ALL_TIME, (string _error)=>{
        //                 Debug.Log("Closed leaderboard UI. Error: " + _error);
        //             });
        //     }
        // }else{
        //     Debug.Log("=== showleaderboard game service not available");
        // }
#endif
    }

    public void ShowAchievements(){
#if UNITY_ANDROID

        // if( m_isGameServiceAvailable ){

        //     if( !NPBinding.GameServices.LocalUser.IsAuthenticated ){
        //         Debug.Log("=== user not authenticated, prepare auth first");
        //         NPBinding.GameServices.LocalUser.Authenticate((bool _success, string _error)=>{
        //                 if (_success)
        //                 {
        //                     Debug.Log("Sign-In Successfully");
        //                     Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser.ToString());

        //                     NPBinding.GameServices.ShowAchievementsUI((string _errorAch)=>{
        //                             Debug.Log("Closed achivement UI. Error: " + _errorAch);
        //                         });
        //                 }
        //                 else
        //                 {
        //                     Debug.Log("Sign-In Failed with error " + _error);
        //                 }
        //             });
        //     }else{
        //         NPBinding.GameServices.ShowAchievementsUI((string _errorAch)=>{
        //                 Debug.Log("Closed achivement UI. Error: " + _errorAch);
        //             });
        //     }
        // }else{
        //     Debug.Log("=== showleaderboard game service not available");
        // }
#endif
    }

    public void UpdateScoreLeaderboard( int score ){
#if UNITY_ANDROID
        SendScoreToLeaderboard(m_GameData.m_GlobalLeaderboardIdAndroid, score);
#elif UNITY_IOS
        SendScoreToLeaderboard(m_GameData.m_GlobalLeaderboardIdIOS, score);
#endif
    }

    public void UpdateDistanceLeaderboard( int score ){
#if UNITY_ANDROID
        SendScoreToLeaderboard(m_GameData.m_GlobalDistanceLeaderboardIdAndroid, score);
#elif UNITY_IOS
        SendScoreToLeaderboard(m_GameData.m_GlobalDistanceLeaderboardIdIOS, score);
#endif
    }

    public void UpdateMultiplayerLeaderboard( int score ){
#if UNITY_ANDROID
        SendScoreToLeaderboard(m_GameData.m_GlobalMultiplayerLeaderboardIdAndroid, score);
#elif UNITY_IOS
        SendScoreToLeaderboard(m_GameData.m_GlobalMultiplayerLeaderboardIdIOS, score);
#endif
    }

    public void UpdateAchivement( string achivementId, double score){
#if UNITY_ANDROID
        // if( m_isGameServiceAvailable ){
        //     NPBinding.GameServices.ReportProgressWithGlobalID(achivementId, score, (bool _success, string _error)=>{
        //             if (_success)
        //             {
        //                 Debug.Log(string.Format("Request to report score to achivement with GID= {0} finished successfully.", achivementId));
        //                 Debug.Log(string.Format("New score= {0}.", score));
        //             }
        //             else
        //             {
        //                 Debug.Log(string.Format("Request to report score to achivement with GID= {0} failed.", achivementId));
        //                 Debug.Log(string.Format("Error= {0}.", _error));
        //             }
        //         });
        // }
#endif
    }


    void SendScoreToLeaderboard( string leaderboardId, int score ){
        Debug.Log("reporting new score to " + leaderboardId + ": " + score.ToString());
        Social.ReportScore( score, leaderboardId, (bool success) => {
        // handle success or failure
        Debug.Log(success);
        } );
    }

    public void OnProductPurchased( string productId, StoreItem si ){
        Debug.Log(productId);
        GameShop gameShop = FindObjectOfType<GameShop>();
        HomeScreenController hs = FindObjectOfType<HomeScreenController>();

#if !UNITY_IOS
        if( !m_AdsRemoverPurchased ){
            m_AdsRemoverPurchased = true;
        }
        if( hs != null ){
            hs.RemoveRemoveAdsButton();
        }
#endif

        if( productId == "remove_ads" ){
            m_AdsRemoverPurchased = true;
            if( hs != null ){
                hs.RemoveRemoveAdsButton();
            }
        }
        if( gameShop != null ){
            if( productId == "starter_pack" ){
                if( m_StarterPackAvailable ){
                    m_StarterPackAvailable = false;
                    GlobalGameManager.instance.m_Coins += si.m_CoinsNum;
                    GlobalGameManager.instance.m_Potions += si.m_PotionsNum;
                    gameShop.RefreshUI();
                }
            }else if( productId == "double_coins" ){
                if( !m_DoubleCoinsPurchased ){
                    m_DoubleCoinsPurchased = true;
                    GlobalGameManager.instance.m_Coins += si.m_CoinsNum;
                    GlobalGameManager.instance.m_Potions += si.m_PotionsNum;
                    gameShop.RefreshUI();
                }
            }else{
                GlobalGameManager.instance.m_Coins += si.m_CoinsNum;
                GlobalGameManager.instance.m_Potions += si.m_PotionsNum;
                gameShop.RefreshUI();
            }

        }else{
            Debug.Log("Warning. GameShop wasn't found. Skip UI refresh");
        }

        GameOverPopup go = FindObjectOfType<GameOverPopup>();
        if( go != null ){
            go.Refresh();
        }
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneFullyLoaded;
    }
         
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneFullyLoaded;
    }

    public void OnSceneFullyLoaded( Scene scene, LoadSceneMode mode ){
        if( scene.buildIndex == 0 ){
            //reset multiplayer mode at home screen
            m_IsMultiplayerGame = false;
        }
    }

    public void ToggleFPS(){
        FPSDisplay display = GetComponent<FPSDisplay>();
        display.enabled = !display.enabled;
    }

    // public void RequestInterstitial()
    // {
    //     #if UNITY_ANDROID
    //     string adUnitId = "ca-app-pub-5316161378481999/3773547468";
    //     #elif UNITY_IPHONE
    //     string adUnitId = "ca-app-pub-5316161378481999/5250280668";
    //     #else
    //     string adUnitId = "unexpected_platform";
    //     #endif

    //     // Initialize an InterstitialAd.
    //     Debug.Log("--- interstitial adid " + adUnitId);
    //     interstitial = new InterstitialAd(adUnitId);

    //     // Create an empty ad request.
    //     // AdRequest request = new AdRequest.Builder().Build();

    //     //test
    //     AdRequest request = new AdRequest.Builder()
    //         .AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
    //         // .AddTestDevice("32DB6651E0E1FEC6")  // My test device.
    //         .AddTestDevice("DFC6FBAC6FC4D473BF035ECF93959EDC")  // My test device.
    //         .Build();
    //     //end test

    //     // Load the interstitial with the request.
    //     interstitial.LoadAd(request);
    // }

    // public void ShowInterstitial()
    // {
    //     Debug.Log("ShowInterstitial");
    //     if (interstitial.IsLoaded()) {
    //         Debug.Log("Loaded");
    //         interstitial.Show();
    //         // interstitial.Destroy();
    //     }else{
    //         Debug.Log("Not loaded");
    //     }
    // }

}
